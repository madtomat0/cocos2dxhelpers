//
//  MTAchievementData.hpp
//  WordTiles
//
//  Created by Pavel on 24/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTAchievementData_hpp
#define MTAchievementData_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../../MTDefines.h"

class MTAchievementData : public cocos2d::Ref {
public:
	~MTAchievementData();
	
	CREATE_FUNC(MTAchievementData);
	
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _name, Name);
	
	MTAchievementData* setup();
	
private:
	MTAchievementData();
	bool init() {return true;};
	
};

#endif /* MTAchievementData_hpp */
