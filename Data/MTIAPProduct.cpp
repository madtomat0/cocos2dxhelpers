//
//  MTIAPProduct.cpp
//  WordTiles
//
//  Created by Pavel on 22/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTIAPProduct.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTIAPProduct::MTIAPProduct()
: _productId("")
, _name("")
, _type(Nonconsumable)
, _title("")
, _description("")
, _localizedPrice("")
, _canBePurchased(false)
{
	
}

MTIAPProduct::~MTIAPProduct() {
	
	//CCLOG("~MTIAPProduct deleted");
}

// -------------------------------------
// MARK: Setup

MTIAPProduct* MTIAPProduct::setup(const sdkbox::Product& product) {
	
	_productId = product.id;
	_name = product.name;
	_type = (product.type == sdkbox::IAP_Type::CONSUMABLE) ? Consumable : Nonconsumable;
	_title = product.title;
	_description = product.description;
	_localizedPrice = StringUtils::format("%.2f %s", product.priceValue, product.currencyCode.c_str());//product.price + product.currencyCode;
	_canBePurchased = (product.priceValue > 0);
	
	return this;
}

cocos2d::Vector<MTIAPProduct*> MTIAPProduct::mapProducts(const std::vector<sdkbox::Product>& products) {
	auto mappedProducts = Vector<MTIAPProduct*>();
	for (auto product : products) {
		mappedProducts.pushBack(MTIAPProduct::create()->setup(product));
	}
	
	return mappedProducts;
}
