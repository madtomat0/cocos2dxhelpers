//
//  MTIAPProduct.hpp
//  WordTiles
//
//  Created by Pavel on 22/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTIAPProduct_hpp
#define MTIAPProduct_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../../MTDefines.h"
#include "PluginIAP/PluginIAP.h"

class MTIAPProduct : public cocos2d::Ref {
public:
	~MTIAPProduct();
	
	enum Type { Consumable = 0, Nonconsumable };
	
	CREATE_FUNC(MTIAPProduct);
	
	CC_SYNTHESIZE_READONLY(std::string, _productId, ProductId);
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _name, Name);
	CC_SYNTHESIZE_READONLY(MTIAPProduct::Type, _type, Type);
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _title, Title);
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _description, Description);
	CC_SYNTHESIZE_READONLY(std::string, _localizedPrice, LocalizedPrice);
	CC_SYNTHESIZE_READONLY(bool, _canBePurchased, CanBePurchased);
	
	MTIAPProduct* setup(const sdkbox::Product& product);
	static cocos2d::Vector<MTIAPProduct*> mapProducts(const std::vector<sdkbox::Product>& products);
	
protected:
	bool init() {return true;};
	MTIAPProduct();
	
private:
	
};

#endif /* MTIAPProduct_hpp */
