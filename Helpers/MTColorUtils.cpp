//
//  MTColorUtils.cpp
//  FillTheWords-mobile
//
//  Created by Pavel on 27/09/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTColorUtils.h"
#include <string>
#include <cstdio>
#include <cstdlib>

USING_NS_CC;

namespace MTColorUtils
{
	
cocos2d::Color3B colorFromHexString(const std::string& hexString) {
	int base = 16;
	
	unsigned int r = (unsigned int)strtoul(hexString.substr(0, 2).c_str(), NULL, base);
	unsigned int g = (unsigned int)strtoul(hexString.substr(2, 2).c_str(), NULL, base);
	unsigned int b = (unsigned int)strtoul(hexString.substr(4, 2).c_str(), NULL, base);
	
	return Color3B(r, g, b);
}
	
	
} // namespace MTColorUtils {
