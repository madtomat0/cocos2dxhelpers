//
//  MTAdsHelper.cpp
//  FillTheWords
//
//  Created by Pavel on 20/09/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTAdsHelper.h"
#include "cocos2d.h"

// -------------------------------------
// MARK: Ctors

MTAdsHelper::MTAdsHelper()
: _adsDisabled(false)
, _bannerIsVisible(false)
{
	
}

MTAdsHelper::~MTAdsHelper() {
	
	CCLOG("~MTAdsHelper deleted");
}

// -------------------------------------
// MARK: Initialization

// singleton stuff
static MTAdsHelper* s_sharedInstance = nullptr;

MTAdsHelper* MTAdsHelper::getInstance() {
	if (!s_sharedInstance) {
		s_sharedInstance = new (std::nothrow) MTAdsHelper();
		CCASSERT(s_sharedInstance, "FATAL: Not enough memory");
		s_sharedInstance->init();
	}
	
	return s_sharedInstance;
}

bool MTAdsHelper::init() {
	CCLOG("MTAdsHelper init started");
	
	
	CCLOG("MTAdsHelper init finished");
	
	return true;
}

// -------------------------------------
// MARK: Accessors

void MTAdsHelper::setAdsDisabled(bool adsDisabled) {
	_adsDisabled = adsDisabled;
}

// -------------------------------------
// MARK: General Methods - Android

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "platform/android/jni/JniHelper.h"
#include <jni.h>
//#include <android/log.h>

void MTAdsHelper::cacheBanner() {
	CCLOG("!!!!!!!! This method does nothing on android for now");
}

void MTAdsHelper::hideBanner() {
	if (_adsDisabled || !_bannerIsVisible) {
		return;
	}
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "hideBanner", "()V")) {
		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
	
	_bannerIsVisible = false;
}

void MTAdsHelper::showBanner(int bannerPlaceholderHeight) {
	if (_adsDisabled || _bannerIsVisible) {
		return;
	}
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "showBanner", "(I)V")) {
		t.env->CallStaticVoidMethod(t.classID, t.methodID, bannerPlaceholderHeight);
		t.env->DeleteLocalRef(t.classID);
	}
	
	_bannerIsVisible = true;
}

float MTAdsHelper::getBannerHeight() {
	CCLOG("!!!!!!!! This method does nothing on android for now");
	return 0.0;
}

void MTAdsHelper::preloadInterstitial() {
	if (_adsDisabled) {
		return;
	}
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "preloadInterstitial", "()V")) {
		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
}

void MTAdsHelper::showInterstitial() {
	if (_adsDisabled) {
		return;
	}
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, AppActivityClassName, "showInterstitialAd", "()V")) {
		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
}

// -------------------------------------
// MARK: General Methods - iOS

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "../../../proj.ios_mac/ios/Helpers/MTAdsWrapper.h"

void MTAdsHelper::cacheBanner() {
	MTAdsWrapper::cacheBanner();
}

void MTAdsHelper::hideBanner() {
	if (_adsDisabled || !_bannerIsVisible) {
		return;
	}
	
	CCLOG("hideBanner() IOS called");
	MTAdsWrapper::hideBanner();
	_bannerIsVisible = false;
}


void MTAdsHelper::showBanner(int bannerPlaceholderHeight) {
	if (_adsDisabled || _bannerIsVisible) {
		return;
	}
	
	CCLOG("showBanner() IOS called");
	MTAdsWrapper::showBanner();
	_bannerIsVisible = true;
}

float MTAdsHelper::getBannerHeight() {
	return MTAdsWrapper::getBannerHeight();
}

void MTAdsHelper::preloadInterstitial() {
	if (_adsDisabled) {
		return;
	}
	
	MTAdsWrapper::preloadInterstitial();
}

void MTAdsHelper::showInterstitial() {
	if (_adsDisabled) {
		return;
	}
	
	MTAdsWrapper::showInterstitial();
}

// -------------------------------------
// MARK: General Methods - other

#else

void MTAdsHelper::hideBanner() {
}

void MTAdsHelper::showBanner() {
}

void MTAdsHelper::preloadInterstitial() {
}

void MTAdsHelper::showInterstitial() {
}

#endif
