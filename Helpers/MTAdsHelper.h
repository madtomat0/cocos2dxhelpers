//
//  MTAdsHelper.hpp
//  FillTheWords
//
//  Created by Pavel on 20/09/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTAdsHelper_hpp
#define MTAdsHelper_hpp

#include <stdio.h>

class MTAdsHelper {
public:
	/** returns a shared instance */
	static MTAdsHelper* getInstance();
	
	void setAdsDisabled(bool adsDisabled);
	
	void cacheBanner();
	
	void hideBanner();
	void showBanner(int bannerPlaceholderHeight);
	
	float getBannerHeight();
	
	void preloadInterstitial();
	void showInterstitial();
	
private:
	const char* AppActivityClassName = "org/cocos2dx/cpp/AppActivity";
	
	bool init();
	MTAdsHelper();
	~MTAdsHelper();
	
	bool _adsDisabled;
	bool _bannerIsVisible;
};

#endif /* MTAdsHelper_hpp */
