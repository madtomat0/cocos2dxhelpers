//
//  MTNotificationListener.h
//  FillTheWords
//
//  Created by Paul Gorbunov on 12/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTNotificationListener_h
#define MTNotificationListener_h

class MTNotificationListener {
public:
	virtual void localNotificationDidReceive(const std::string& notificationKey, const std::string& message) = 0;
};

#endif /* MTNotificationListener_h */
