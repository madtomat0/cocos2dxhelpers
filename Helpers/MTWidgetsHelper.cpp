//
//  MTWidgetsHelper.cpp
//  GoldDigger
//
//  Created by MAD Tomato on 15.02.15.
//
//

#include "MTWidgetsHelper.h"
#include "MTScreenHelper.h"
#include "../../MTDefines.h"
#include "../../Model/Managers/SoundsManager.h"

USING_NS_CC;
using namespace cocos2d::ui;

// -------------------------------------
// MARK: Setup Methods

MTButton* MTWidgetsHelper::setupBtn(const std::string& frameName, Vec2 pos, Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor) {
	
	auto btn = MTButton::create(frameName, frameName, frameName, Button::TextureResType::PLIST);
	btn->setTouchEnabled(true);
	btn->setPressedActionEnabled(true);
	btn->setZoomScale(-0.1);
	btn->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	btn->setPosition(pos);
	parentNode->addChild(btn);
	
	if (callback) {
		btn->addTouchEventListener([callback](Ref* sender,Widget::TouchEventType eventType){
			if (eventType == Widget::TouchEventType::ENDED) {
				SoundsManager::getInstance()->playSound(SoundsManager::SoundType::Tap);
				callback();
			}
		});
	}
	
	if (useScaleFactor) {
		float scaleFactor = MTScreenHelper::getMenuScaleFactor();
		btn->setScale(scaleFactor);
	}
	
	return btn;
}

MTButton* MTWidgetsHelper::setupBtn(const std::string& frameName, const std::string& selectedFrameName, cocos2d::Vec2 pos, cocos2d::Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor) {
	
	auto btn = MTButton::create(frameName, selectedFrameName, frameName, Button::TextureResType::PLIST);
	btn->setTouchEnabled(true);
	btn->setPressedActionEnabled(true);
	btn->setZoomScale(-0.1);
	btn->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	btn->setPosition(pos);
	parentNode->addChild(btn);
	
	if (callback) {
		btn->addTouchEventListener([callback](Ref* sender,Widget::TouchEventType eventType){
			if (eventType == Widget::TouchEventType::ENDED) {
				SoundsManager::getInstance()->playSound(SoundsManager::SoundType::Tap);
				callback();
			}
		});
	}
	
	if (useScaleFactor) {
		float scaleFactor = MTScreenHelper::getMenuScaleFactor();
		btn->setScale(scaleFactor);
	}
	
	return btn;
}

MTCheckbox* MTWidgetsHelper::setupCheckboxBtn(const std::string& frameName, const std::string& selectedFrameName, cocos2d::Vec2 pos, cocos2d::Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor) {
	
	auto checkbox = MTCheckbox::create(selectedFrameName, frameName, [callback](cocos2d::Ref*){
		if (callback) {
			callback();
		}
	});
	
	checkbox->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	checkbox->setPosition(pos);
	parentNode->addChild(checkbox);
	
	if (useScaleFactor) {
		float scaleFactor = MTScreenHelper::getMenuScaleFactor();
		checkbox->setScale(scaleFactor);
	}
	
	return checkbox;
}

MTText* MTWidgetsHelper::setupLabelBtn(const std::string& btnText, const std::string& fontName, float fontSize, Vec2 pos, Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor) {
	
	auto btn = MTText::create(btnText, fontName, fontSize);
	btn->setTouchEnabled(true);
	btn->setTouchScaleChangeEnabled(true);
	btn->setOnTouchZoom(-0.1);
	btn->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	btn->setPosition(pos);
	parentNode->addChild(btn);
	
	if (callback) {
		btn->addTouchEventListener([callback](Ref* sender,Widget::TouchEventType eventType){
			if (eventType == Widget::TouchEventType::ENDED) {
				SoundsManager::getInstance()->playSound(SoundsManager::SoundType::Tap);
				callback();
			}
		});
	}
	
	if (useScaleFactor) {
		float scaleFactor = MTScreenHelper::getMenuScaleFactor();
		btn->setScale(scaleFactor);
	}
	
	return btn;
}

MTButton* MTWidgetsHelper::setupLabelBtnWithBg(const std::string& btnText, const std::string& bgImageName, const Size& btnSize, const std::string& fontName, float fontSize, Vec2 pos, Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor) {
	
	auto btn = MTWidgetsHelper::setupBtn(bgImageName, pos, parentNode, callback, useScaleFactor);
	btn->setCascadeColorEnabled(false);
	btn->setTitleFontName(fontName);
	btn->setScale9Enabled(true);
	btn->setTitleText(btnText);
	btn->setTitleFontSize(fontSize);
	btn->setContentSize(btnSize);
	
	return btn;
}

Layout* MTWidgetsHelper::setupLayout(const cocos2d::Size& size, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, const cocos2d::Color3B color, bool useColor) {
	
	auto layout = Layout::create();
	layout->setContentSize(size);
	layout->setAnchorPoint(anchor);
	layout->setPosition(pos);
	if (useColor) {
		layout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
		layout->setBackGroundColor(color);
	}
	
	if (parentNode) {
		parentNode->addChild(layout);
	}
	
	return layout;
}

Text* MTWidgetsHelper::setupLabel(const std::string& textContent, const std::string& fontName, float fontSize, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, const cocos2d::Color3B color, bool useScaleFactor) {
	
	auto label = Text::create(textContent, fontName, fontSize);
	label->setAnchorPoint(anchor);
	label->setPosition(pos);
	label->setColor(color);
	parentNode->addChild(label);
	
	if (useScaleFactor) {
		float scaleFactor = MTScreenHelper::getMenuScaleFactor();
		label->setScale(scaleFactor);
	}
	
	return label;
}

Sprite* MTWidgetsHelper::setupSpriteRect(const cocos2d::Size& size, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, const cocos2d::Color3B color) {
	
	auto spriteRect = Sprite::create(MTImgWhiteRectName);
	spriteRect->setTextureRect(Rect(0, 0, size.width, size.height));
	spriteRect->setAnchorPoint(anchor);
	spriteRect->setPosition(pos);
	spriteRect->setColor(color);
	
	if (parentNode) {
		parentNode->addChild(spriteRect);
	}
	
	return spriteRect;
}

Sprite* MTWidgetsHelper::setupSprite(const std::string& frameName, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, bool useScaleFactor) {
	
	auto sprite = Sprite::createWithSpriteFrameName(frameName);
	sprite->setAnchorPoint(anchor);
	sprite->setPosition(pos);
	if (useScaleFactor) {
		float scaleFactor = MTScreenHelper::getMenuScaleFactor();
		sprite->setScale(scaleFactor);
	}
	
	parentNode->addChild(sprite);
	return sprite;
}

// -------------------------------------
// MARK: Utils

const cocos2d::Size MTWidgetsHelper::getSpriteBounds(const std::string& spriteFrameName, bool useScaleFactor) {
	
	auto sprite = Sprite::createWithSpriteFrameName(spriteFrameName);
	
	if (useScaleFactor) {
		float scaleFactor = MTScreenHelper::getMenuScaleFactor();
		sprite->setScale(scaleFactor);
	}
	
	return sprite->getBoundingBox().size;
}
