//
//  MTAppodealHelper.hpp
//  FillTheWords-mobile
//
//  Created by Pavel on 18/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTAppodealHelper_hpp
#define MTAppodealHelper_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "MTAppodealListener.h"

class MTAppodealHelper {
public:
    /** returns a shared instance */
    static MTAppodealHelper* getInstance();
    
    void setup(const std::string& apiKey);
    void setListener(MTAppodealListener* listener);
    
    void showAd(MTAppodeal::AdType adType);
    void hideBanner();
    
    void cacheAd(MTAppodeal::AdType adType);
    bool isReadyForShow(MTAppodeal::AdType adType);
	
	// Helpers
	void callListenerForBannerState(MTAppodeal::AdState bannerState);
	void callListenerForInterstitialState(MTAppodeal::AdState interstitialState);
	void callListenerForRewardedState(MTAppodeal::AdState rewardedState);
    
private:
    const char* AppActivityClassName = "org/cocos2dx/cpp/AppActivity";
	
    // Helpers
	static std::string adNameStringforType(MTAppodeal::AdType adType);
	
    
    bool init();
    MTAppodealHelper();
    ~MTAppodealHelper();
    
    MTAppodealListener* _listener;
};

#endif /* MTAppodealHelper_hpp */
