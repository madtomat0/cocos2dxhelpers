//
//  MTError.cpp
//  Xonix
//
//  Created by MAD Tomato on 20.01.15.
//
//

#include "MTError.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTError::MTError()
: _errorDescription("")
{
	
}

MTError::~MTError() {
	
	CCLOG("~MTError deleted");
}

// -------------------------------------
// MARK: Initialization

MTError* MTError::create(const std::string& errorDescription) {
	MTError* data = new MTError();
	if (data && data->init(errorDescription)) {
		data->autorelease();
		return data;
	}
	CC_SAFE_DELETE(data);
	return NULL;
}

bool MTError::init(const std::string& errorDescription) {
	_errorDescription = errorDescription;
	
	return true;
}
