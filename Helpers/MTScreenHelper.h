//
//  MTScreenHelper.h
//  GoldDigger
//
//  Created by MAD Tomato on 16.02.15.
//
//

#ifndef __GoldDigger__MTScreenHelper__
#define __GoldDigger__MTScreenHelper__

#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>

enum MTScreenType {
	MTScreenTypeINVALID = 0,
	MTScreenTypeTablet,
	MTScreenTypeNormal,
	MTScreenTypeWide
};

class MTScreenHelper {
public:
	static MTScreenType getScreenType();
	static float getMenuScaleFactor();
	static float getViewScaleYFactor();
	static float getDeviceDensityPixel();
	
	/**
	 Returns width in pixels of the "img_whiteRect_single.png" for current screen.
	 */
	static float getUnitValue();
	
	static cocos2d::ui::Margin getSafeAreaMargin();
	
	static float heightPixelsToHeight(float heightPx);
	
private:
	static MTScreenType s_screenType;
	static float s_menuScaleFactor;
	static float s_viewScaleYFactor;
	static float s_unitValue;
	static cocos2d::ui::Margin s_safeAreaMargin;
};

#endif /* defined(__GoldDigger__MTScreenHelper__) */
