//
//  MTSharingHelper.hpp
//  FillTheWords-mobile
//
//  Created by Paul Gorbunov on 07/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTSharingHelper_hpp
#define MTSharingHelper_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "MTSharingListener.h"

class MTSharingHelper : public MTSharingListener {
public:
	/** returns a shared instance */
	static MTSharingHelper* getInstance();
    
    typedef std::function<void (bool success)> SharingCallback;
    
    SharingCallback _sharingCallback;
	
	void initializeVkontakteWithId(const std::string& vkAppId);
	
	void postMessage(const std::string& message, const std::string& photo, const std::string& link, const SharingCallback& callback);
	
private:
	const char* AppActivityClassName = "org/cocos2dx/cpp/AppActivity";
	
	bool init();
	MTSharingHelper();
	~MTSharingHelper();
	
	// MTSharingListener
	virtual void messageWasPostedWithSuccess(bool success);
	
	
};

#endif /* MTSharingHelper_hpp */
