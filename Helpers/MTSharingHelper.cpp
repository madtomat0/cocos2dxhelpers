//
//  MTSharingHelper.cpp
//  FillTheWords-mobile
//
//  Created by Paul Gorbunov on 07/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTSharingHelper.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTSharingHelper::MTSharingHelper()
: _sharingCallback(nullptr)
{
	
}

MTSharingHelper::~MTSharingHelper() {
	_sharingCallback = nullptr;
	
	CCLOG("~MTSharingHelper deleted");
}

// -------------------------------------
// MARK: Initialization

// singleton stuff
static MTSharingHelper* s_sharedInstance = nullptr;

MTSharingHelper* MTSharingHelper::getInstance() {
	if (!s_sharedInstance) {
		s_sharedInstance = new (std::nothrow) MTSharingHelper();
		CCASSERT(s_sharedInstance, "FATAL: Not enough memory");
		s_sharedInstance->init();
	}
	
	return s_sharedInstance;
}

bool MTSharingHelper::init() {
	CCLOG("MTSharingHelper init started");
	
	
	CCLOG("MTSharingHelper init finished");
	
	return true;
}

// -------------------------------------
// MARK: General Methods - Android

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "platform/android/jni/JniHelper.h"
#include <jni.h>
//#include <android/log.h>
#include <string>

const char* SharingHelperClassName = "com.codefabric.helpers.MTSharingHelper";

void MTSharingHelper::initializeVkontakteWithId(const std::string& vkAppId) {
	// Nothing to do here
}

void MTSharingHelper::postMessage(const std::string& message, const std::string& photo, const std::string& link, const SharingCallback& callback) {
	
	_sharingCallback = callback;
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, SharingHelperClassName, "postMessage", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V")) {
		
		jstring stringArg1 = t.env->NewStringUTF(message.c_str());
		jstring stringArg2 = t.env->NewStringUTF(photo.c_str());
		jstring stringArg3 = t.env->NewStringUTF(link.c_str());
		
		t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, stringArg2, stringArg3);
		t.env->DeleteLocalRef(stringArg1);
		t.env->DeleteLocalRef(stringArg2);
		t.env->DeleteLocalRef(stringArg3);
		t.env->DeleteLocalRef(t.classID);
	}
}

extern "C" {
	
	JNIEXPORT void JNICALL Java_com_codefabric_helpers_MTSharingHelper_messagePostedWithSuccess
	(JNIEnv *env, jobject obj, jboolean jsuccess) {
		
		CCLOG("Message posted callback from Java");
		
		bool success = (bool)jsuccess;
		Director::getInstance()->getScheduler()->performFunctionInCocosThread([success](){
			if (MTSharingHelper::getInstance()->_sharingCallback != nullptr) {
				
				MTSharingHelper::getInstance()->_sharingCallback(success);
			}
			
			MTSharingHelper::getInstance()->_sharingCallback = nullptr;
		});
		
	}
	
}

// -------------------------------------
// MARK: General Methods - iOS

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "../../../proj.ios_mac/ios/SocialSharing/MTSharingWrapper.h"

void MTSharingHelper::initializeVkontakteWithId(const std::string& vkAppId) {
	MTSharingWrapper::setListener(this);
	MTSharingWrapper::initializeVkontakte(vkAppId);
}

void MTSharingHelper::postMessage(const std::string& message, const std::string& photo, const std::string& link, const SharingCallback& callback) {
	
	_sharingCallback = callback;
	MTSharingWrapper::postMessage(message, photo, link);
}

// -------------------------------------
// MARK: General Methods - other

#else
#endif

// -------------------------------------
// MARK: MTSharingListener

void MTSharingHelper::messageWasPostedWithSuccess(bool success) {
	if (_sharingCallback) {
		_sharingCallback(success);
		_sharingCallback = nullptr;
	}
}
