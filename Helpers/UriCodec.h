
#ifndef UriCodec_h
#define UriCodec_h

#include "cocos2d.h"

NS_CC_BEGIN

namespace StringUtils
{
    std::string UriDecode(const std::string& sSrc);
    std::string UriEncode(const std::string& sSrc);
}

NS_CC_END

#endif /* UriCodec_h */
