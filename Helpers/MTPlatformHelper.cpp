//
//  MTPlatformHelper.cpp
//  WordTiles
//
//  Created by Pavel on 03/07/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTPlatformHelper.h"
#include "cocos2d.h"

USING_NS_CC;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "platform/android/jni/JniHelper.h"
#include <jni.h>

const char* PlatformHelperClassName = "com.codefabric.helpers.MTPlatformHelper";

std::string MTPlatformHelper::getCurrentOSVersion() {
	JniMethodInfo t;
	std::string versionString("");
	
	if (JniHelper::getStaticMethodInfo(t, PlatformHelperClassName, "getCurrentOSVersionString", "()Ljava/lang/String;")) {
		jstring str = (jstring)t.env->CallStaticObjectMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
		versionString = JniHelper::jstring2string(str);
		t.env->DeleteLocalRef(str);
	}
	
	return versionString;
}

std::string MTPlatformHelper::getDeviceModel() {
	JniMethodInfo t;
	std::string modelString("");
	
	if (JniHelper::getStaticMethodInfo(t, PlatformHelperClassName, "getDeviceModelString", "()Ljava/lang/String;")) {
		jstring str = (jstring)t.env->CallStaticObjectMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
		modelString = JniHelper::jstring2string(str);
		t.env->DeleteLocalRef(str);
	}
	
	return modelString;
}

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "../../../proj.ios_mac/ios/Helpers/MTPlatformHelper_iOS.h"

std::string MTPlatformHelper::getCurrentOSVersion() {
	return MTPlatformHelper_iOS::getCurrentOSVersion();
}

std::string MTPlatformHelper::getDeviceModel() {
	return MTPlatformHelper_iOS::getDeviceModel();
}

#else

#endif

bool MTPlatformHelper::isAndroidOS() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return true;
#else
	return false;
#endif
}

bool MTPlatformHelper::isiPhoneX() {
	Size frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();
	//	1125px × 2436px is iPhoneX
	if ((frameSize.width == 1125 && frameSize.height == 2436) ||
		(frameSize.width == 2436 && frameSize.height == 1125)) {
		
		if (!MTPlatformHelper::isAndroidOS()) {
			return true;
		}
	}
	
	return false;
}

bool MTPlatformHelper::isPortraitScreenOrientation() {
	Size frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();
	return (frameSize.height > frameSize.width);
}
