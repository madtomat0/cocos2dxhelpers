//
//  MTError.h
//  Xonix
//
//  Created by MAD Tomato on 20.01.15.
//
//

#ifndef __Xonix__MTError__
#define __Xonix__MTError__

#include "cocos2d.h"

class MTError : public cocos2d::Ref {
public:
	static MTError* create(const std::string& errorDescription);
	~MTError();
	
	CC_SYNTHESIZE_READONLY_PASS_BY_REF(std::string, _errorDescription, ErrorDescription);
	
	
private:
	bool init(const std::string& errorDescription);
	MTError();
};

#endif /* defined(__Xonix__MTError__) */
