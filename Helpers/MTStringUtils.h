//
//  MTStringUtils.hpp
//  FillTheWords-mobile
//
//  Created by Pavel on 02/10/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTStringUtils_hpp
#define MTStringUtils_hpp

#include <stdio.h>
#include "cocos2d.h"

namespace MTStringUtils
{
	template<typename Out>
	void split(const std::string &s, char delim, Out result) {
		std::stringstream ss;
		ss.str(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			*(result++) = item;
		}
	}
	
	/**
	 Splits string and returns its parts as vector
	 @param s - string to split
	 @param delim - char to split with
	 */
	std::vector<std::string> split(const std::string &s, char delim);
	
	/**
	 Splits path with "/" char and returns new string without last component
	 */
	std::string removeLastComponentFromPath(const std::string& originalPath);
	
	/**
	 Removes substrings from input string
	 @param s - input string
	 @param p - substring to remove
	 */
	template<typename T>
	void removeSubstrings(std::basic_string<T>& s, const std::basic_string<T>& p) {
		typename std::basic_string<T>::size_type n = p.length();
		
		for (typename std::basic_string<T>::size_type i = s.find(p);
			 i != std::basic_string<T>::npos;
			 i = s.find(p))
			s.erase(i, n);
	}
	
	bool stringContainsSubstring(const std::string& inputString, const std::string& substring);
	
	/**
	 Returns "Android" or "iOS" string based on current platform
	 */
	std::string getPlatformString();
	
	/**
	 Creates writable path used by Corona based apps
	 */
	std::string getCoronaWritablePath();
	
	/**
	 Divides string into characters and returns vector of std::strings
	 */
	cocos2d::ValueVector createLettersVector(const std::string& originalString);
}

#endif /* MTStringUtils_hpp */
