//
//  MTNotificationHelper.cpp
//  FillTheWords-mobile
//
//  Created by Paul Gorbunov on 11/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTNotificationHelper.h"
#include "MTHelperUtils.h"

USING_NS_CC;

const std::string MTNotificationHelper::notificationDidReceive = "MTHelper_NotificationDidReceive";

// -------------------------------------
// MARK: Ctors

MTNotificationHelper::MTNotificationHelper()
: _receivedNotifications(ValueMapNull)
{
	
}

MTNotificationHelper::~MTNotificationHelper() {
	
	
	CCLOG("~MTNotificationHelper deleted");
}

// -------------------------------------
// MARK: Initialization

// singleton stuff
static MTNotificationHelper* s_sharedInstance = nullptr;

MTNotificationHelper* MTNotificationHelper::getInstance() {
	if (!s_sharedInstance) {
		s_sharedInstance = new (std::nothrow) MTNotificationHelper();
		CCASSERT(s_sharedInstance, "FATAL: Not enough memory");
		s_sharedInstance->init();
	}
	
	return s_sharedInstance;
}

bool MTNotificationHelper::init() {
	CCLOG("MTNotificationHelper init started");
	
	CCLOG("MTNotificationHelper init finished");
	
	return true;
}

// -------------------------------------
// MARK: General Methods - Android

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <string>

const char* MTNotificationHelperClassName = "com.codefabric.helpers.MTNotificationHelper";

void MTNotificationHelper::prepareListener() {
	
}

void MTNotificationHelper::scheduleLocalNotification(float periodSeconds, const std::string& messageToShow, const std::string& notificationKey) {
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, MTNotificationHelperClassName, "scheduleLocalNotification", "(FLjava/lang/String;Ljava/lang/String;)V")) {
		
		jstring stringArg1 = t.env->NewStringUTF(messageToShow.c_str());
		jstring stringArg2 = t.env->NewStringUTF(notificationKey.c_str());
		
		t.env->CallStaticVoidMethod(t.classID, t.methodID, periodSeconds, stringArg1, stringArg2);
		t.env->DeleteLocalRef(stringArg1);
		t.env->DeleteLocalRef(stringArg2);
		t.env->DeleteLocalRef(t.classID);
	}
}

extern "C" {
	
	JNIEXPORT void JNICALL Java_com_codefabric_helpers_MTNotificationHelper_receivedNotification
	(JNIEnv *env, jobject obj, jstring jnotificationKey) {
		
		CCLOG("Message posted callback from Java");
		
		const char* nativeString = env->GetStringUTFChars(jnotificationKey, NULL);//env->GetStringUTFChars(env, jnotificationKey, 0);
		std::string notificationKeyString = std::string(nativeString);
		
		Director::getInstance()->getScheduler()->performFunctionInCocosThread([notificationKeyString](){
			
			MTNotificationHelper::getInstance()->localNotificationDidReceive(notificationKeyString, "");
		});
		
		env->ReleaseStringUTFChars(jnotificationKey, nativeString);
	}
	
}

// -------------------------------------
// MARK: General Methods - iOS

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "../../../proj.ios_mac/ios/Helpers/Notifications/MTNotificationWrapper.h"

void MTNotificationHelper::prepareListener() {
	MTNotificationWrapper::setListener(this);
}

void MTNotificationHelper::scheduleLocalNotification(float periodSeconds, const std::string& messageToShow, const std::string& notificationKey) {
	MTNotificationWrapper::scheduleLocalNotification(periodSeconds, messageToShow, notificationKey);
}

// -------------------------------------
// MARK: General Methods - other

#else
#endif

// -------------------------------------
// MARK: General Methods

bool MTNotificationHelper::hasNotification(const std::string& notificationKey) const {
	auto it = _receivedNotifications.find(notificationKey);
	return (it != _receivedNotifications.end());
}

bool MTNotificationHelper::hasEmptyNotification() const {
	return this->hasNotification("empty");
}

void MTNotificationHelper::clearAllNotifications() {
	_receivedNotifications.clear();
}

// -------------------------------------
// MARK: MTNotificationListener

void MTNotificationHelper::localNotificationDidReceive(const std::string& notificationKey, const std::string& message) {
	
	if (notificationKey != "") {
		_receivedNotifications[notificationKey] = Value(message);
		
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		dispatcher->dispatchCustomEvent(MTNotificationHelper::notificationDidReceive);
	} else {
		// Means that notification was received, but user tapped on app icon instead of notification banner
		// Save empty notification (means that original notification should be rescheduled instantly)
		_receivedNotifications["empty"] = Value(message);
		
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		dispatcher->dispatchCustomEvent(MTNotificationHelper::notificationDidReceive);
	}
	
}
