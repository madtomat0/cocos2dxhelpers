//
//  MTScreenHelper.cpp
//  GoldDigger
//
//  Created by MAD Tomato on 16.02.15.
//
//

#include "MTScreenHelper.h"
#include "../../AppDelegate.h"
#include "MTPlatformHelper.h"

USING_NS_CC;
using namespace cocos2d::ui;

MTScreenType MTScreenHelper::s_screenType = MTScreenTypeINVALID;

MTScreenType MTScreenHelper::getScreenType() {
	if (s_screenType == MTScreenTypeINVALID) {
		Size frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();
//		float screenRatio = frameSize.height / frameSize.width;
//		if (screenRatio < 1.0) {
//			screenRatio = frameSize.width / frameSize.height;
//		}
//		
//		if (screenRatio <= 1.34) { // iPad ratio
//			s_screenType = MTScreenTypeTablet;
//		} else if (screenRatio <= 1.5) { // iPhone4 ratio
//			s_screenType = MTScreenTypeNormal;
//		} else  {
//			s_screenType = MTScreenTypeWide;
//		}
		
		float heightInDensityPixels = frameSize.height / MTScreenHelper::getDeviceDensityPixel();
		if (heightInDensityPixels <= 480) {
			s_screenType = MTScreenTypeNormal;
		} else if (heightInDensityPixels <= 1000) {
			s_screenType = MTScreenTypeWide;
		} else {
			s_screenType = MTScreenTypeTablet;
		}
		
	}
	
	return s_screenType;
}

float MTScreenHelper::s_menuScaleFactor = -1.0;

float MTScreenHelper::getMenuScaleFactor() {
	if (s_menuScaleFactor == -1.0) {
		if (MTScreenHelper::getScreenType() == MTScreenTypeTablet) {
			s_menuScaleFactor = (AppDelegate::designResolutionIsForTablets()) ? 1.0 : 0.7;
		} else {
			s_menuScaleFactor = (AppDelegate::designResolutionIsForTablets()) ? 1.5 : 1.0;
		}
	}
	
	return s_menuScaleFactor;
}

float MTScreenHelper::getDeviceDensityPixel() {
	float densityPixel = 1.0;
	auto glview = Director::getInstance()->getOpenGLView();
	
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	float contentScaleFactor = glview->getContentScaleFactor();
	CCLOG("ContentScaleFactor = %f", contentScaleFactor);
	densityPixel = contentScaleFactor;
	
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	int deviceDPI = Device::getDPI();
	CCLOG("DeviceDPI = %d", deviceDPI);
	densityPixel = static_cast<float>(deviceDPI) / 160.0;
	CCLOG("densityPixel = %f", densityPixel);
#endif
	
	return densityPixel;
}

float MTScreenHelper::s_viewScaleYFactor = -1.0;

float MTScreenHelper::getViewScaleYFactor() {
	if (s_viewScaleYFactor == -1.0) {
		auto glview = Director::getInstance()->getOpenGLView();
		float densityPixel = MTScreenHelper::getDeviceDensityPixel();
		CCLOG("ScaleY = %f", glview->getScaleY());
		
		s_viewScaleYFactor = glview->getScaleY() / densityPixel;
	}
	
	return s_viewScaleYFactor;
}

float MTScreenHelper::s_unitValue = -1.0;

float MTScreenHelper::getUnitValue() {
	if (s_unitValue == -1.0) {
		auto squareSprite = Sprite::createWithSpriteFrameName("img_whiteRect_single.png");
		s_unitValue = squareSprite->getBoundingBox().size.width;
	}
	
	return s_unitValue;
}

float MTScreenHelper::heightPixelsToHeight(float heightPx) {
	float densityPixel = MTScreenHelper::getDeviceDensityPixel();
	float height = heightPx / densityPixel;
	
	float scaleYFactor = MTScreenHelper::getViewScaleYFactor();
	height = height / scaleYFactor;
	
	return height;
}

#define _sh(pixelHeight) MTScreenHelper::heightPixelsToHeight(pixelHeight)

cocos2d::ui::Margin MTScreenHelper::s_safeAreaMargin = Margin(-1, 0, 0, 0);

cocos2d::ui::Margin MTScreenHelper::getSafeAreaMargin() {
	if (s_safeAreaMargin.left == -1) {
		if (MTPlatformHelper::isiPhoneX()) {
			if (MTPlatformHelper::isPortraitScreenOrientation()) {
				//return Margin(0.0, _sh(132.0), 0.0, _sh(102.0)); // actual iPhoneX safe area
				return Margin(0.0, _sh(100.0), 0.0, _sh(102.0)); // top margin slightly reduced
			} else {
				return Margin(_sh(44.0), _sh(32.0), _sh(44.0), _sh(21.0));
			}
			
		} else {
			return Margin(0.0, 0.0, 0.0, 0.0);
		}
	}
	
	return s_safeAreaMargin;
}
