//
//  MTTimeUtils.hpp
//  FillTheWords-mobile
//
//  Created by Pavel on 04/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTTimeUtils_hpp
#define MTTimeUtils_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <chrono>
#include <ctime>

namespace MTTimeUtils
{
	double timePointToSeconds(const std::chrono::steady_clock::time_point& timePoint);
	double systemTimePointToSeconds(const std::chrono::system_clock::time_point& timePoint);
    
    std::chrono::steady_clock::time_point timePointFromSeconds(double seconds);
	std::chrono::system_clock::time_point systemTimePointFromSeconds(double seconds);
	
	std::string formattedTime(const std::chrono::steady_clock::time_point& timePoint);
	std::string systemFormattedTime(const std::chrono::system_clock::time_point& timePoint);
	
	std::string formattedTime(double seconds);
	std::string systemFormattedTime(double seconds);
}

#endif /* MTTimeUtils_hpp */
