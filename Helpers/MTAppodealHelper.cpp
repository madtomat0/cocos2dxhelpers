//
//  MTAppodealHelper.cpp
//  FillTheWords-mobile
//
//  Created by Pavel on 18/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTAppodealHelper.h"
#include "MTHelperUtils.h"

USING_NS_CC;

//const std::string MTAppodealHelper::notificationDidReceive = "MTHelper_NotificationDidReceive";

// -------------------------------------
// MARK: Ctors

MTAppodealHelper::MTAppodealHelper()
: _listener(nullptr)
{
	
}

MTAppodealHelper::~MTAppodealHelper() {
	_listener = nullptr;
	
	CCLOG("~MTAppodealHelper deleted");
}

// -------------------------------------
// MARK: Initialization

// singleton stuff
static MTAppodealHelper* s_sharedInstance = nullptr;

MTAppodealHelper* MTAppodealHelper::getInstance() {
	if (!s_sharedInstance) {
		s_sharedInstance = new (std::nothrow) MTAppodealHelper();
		CCASSERT(s_sharedInstance, "FATAL: Not enough memory");
		s_sharedInstance->init();
	}
	
	return s_sharedInstance;
}

bool MTAppodealHelper::init() {
	return true;
}

// -------------------------------------
// MARK: General Methods - Android

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#include "platform/android/jni/JniHelper.h"
#include <jni.h>
#include <string>

const char* MTAppodealHelperClassName = "com.codefabric.helpers.MTAppodealHelper";

void MTAppodealHelper::setup(const std::string& apiKey) {
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, MTAppodealHelperClassName, "setup", "(Ljava/lang/String;)V")) {

		jstring stringArg1 = t.env->NewStringUTF(apiKey.c_str());

		t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
		t.env->DeleteLocalRef(stringArg1);
		t.env->DeleteLocalRef(t.classID);
	}
}

void MTAppodealHelper::setListener(MTAppodealListener* listener) {
	_listener = listener;
	
}

void MTAppodealHelper::showAd(MTAppodeal::AdType adType) {
	std::string adName = MTAppodealHelper::adNameStringforType(adType);
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, MTAppodealHelperClassName, "showAd", "(Ljava/lang/String;)V")) {
		
		jstring stringArg1 = t.env->NewStringUTF(adName.c_str());
		
		t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
		t.env->DeleteLocalRef(stringArg1);
		t.env->DeleteLocalRef(t.classID);
	}
}

void MTAppodealHelper::hideBanner() {
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, MTAppodealHelperClassName, "hideBanner", "()V")) {
		
		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
}

void MTAppodealHelper::cacheAd(MTAppodeal::AdType adType) {
	std::string adName = MTAppodealHelper::adNameStringforType(adType);
	
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, MTAppodealHelperClassName, "cacheAd", "(Ljava/lang/String;)V")) {
		
		jstring stringArg1 = t.env->NewStringUTF(adName.c_str());
		
		t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
		t.env->DeleteLocalRef(stringArg1);
		t.env->DeleteLocalRef(t.classID);
	}
}

bool MTAppodealHelper::isReadyForShow(MTAppodeal::AdType adType) {
	std::string adName = MTAppodealHelper::adNameStringforType(adType);
	bool isReady = false;
	
	cocos2d::JniMethodInfo t;
	if (JniHelper::getStaticMethodInfo(t, MTAppodealHelperClassName, "isReadyForShow", "(Ljava/lang/String;)Z")) {
		
		jstring stringArg1 = t.env->NewStringUTF(adName.c_str());
		
		//jboolean j_isReady = (jboolean)t.env->CallStaticObjectMethod(t.classID, t.methodID, stringArg1);
        jboolean j_isReady = (jboolean)t.env->CallStaticBooleanMethod(t.classID, t.methodID, stringArg1);
		t.env->DeleteLocalRef(t.classID);
		
		isReady = (bool)j_isReady;
		
		t.env->DeleteLocalRef(stringArg1);
		//t.env->DeleteLocalRef(j_isReady); // ??? maybe not delete??
	}
	
	return isReady;
}

extern "C" {

	JNIEXPORT void JNICALL Java_com_codefabric_helpers_MTAppodealHelper_onBannerCallback
	(JNIEnv *env, jobject obj, jint jBannerState) {

		CCLOG("on Banner callback from Java");

//		const char* nativeString = env->GetStringUTFChars(jnotificationKey, NULL);//env->GetStringUTFChars(env, jnotificationKey, 0);
//		std::string notificationKeyString = std::string(nativeString);
		
		int bannerState = (int)jBannerState;

		Director::getInstance()->getScheduler()->performFunctionInCocosThread([bannerState](){

			MTAppodealHelper::getInstance()->callListenerForBannerState((MTAppodeal::AdState)bannerState);
		});

		//env->ReleaseStringUTFChars(jnotificationKey, nativeString);
	}
	
	JNIEXPORT void JNICALL Java_com_codefabric_helpers_MTAppodealHelper_onInterstitialCallback
	(JNIEnv *env, jobject obj, jint jInterstitialState) {
		
		CCLOG("on Interstitial callback from Java");
		
		//		const char* nativeString = env->GetStringUTFChars(jnotificationKey, NULL);//env->GetStringUTFChars(env, jnotificationKey, 0);
		//		std::string notificationKeyString = std::string(nativeString);
		
		int interstitialState = (int)jInterstitialState;
		
		Director::getInstance()->getScheduler()->performFunctionInCocosThread([interstitialState](){
			
			MTAppodealHelper::getInstance()->callListenerForInterstitialState((MTAppodeal::AdState)interstitialState);
		});
		
		//env->ReleaseStringUTFChars(jnotificationKey, nativeString);
	}
	
	JNIEXPORT void JNICALL Java_com_codefabric_helpers_MTAppodealHelper_onRewardedVideoCallback
	(JNIEnv *env, jobject obj, jint jRewardedState) {
		
		CCLOG("on Rewarded video callback from Java");
		
		//		const char* nativeString = env->GetStringUTFChars(jnotificationKey, NULL);//env->GetStringUTFChars(env, jnotificationKey, 0);
		//		std::string notificationKeyString = std::string(nativeString);
		
		int rewardedState = (int)jRewardedState;
		
		Director::getInstance()->getScheduler()->performFunctionInCocosThread([rewardedState](){
			
			MTAppodealHelper::getInstance()->callListenerForRewardedState((MTAppodeal::AdState)rewardedState);
		});
		
		//env->ReleaseStringUTFChars(jnotificationKey, nativeString);
	}

}

// -------------------------------------
// MARK: General Methods - iOS

#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#include "../../../proj.ios_mac/ios/Helpers/Appodeal/MTAppodealWrapper.h"

void MTAppodealHelper::setup(const std::string& apiKey) {
	MTAppodealWrapper::setup(apiKey);
}

void MTAppodealHelper::setListener(MTAppodealListener* listener) {
	_listener = listener;
	MTAppodealWrapper::setListener(_listener);
}

void MTAppodealHelper::showAd(MTAppodeal::AdType adType) {
	MTAppodealWrapper::showAd(adType);
}

void MTAppodealHelper::hideBanner() {
	MTAppodealWrapper::hideBanner();
}

void MTAppodealHelper::cacheAd(MTAppodeal::AdType adType) {
	MTAppodealWrapper::cacheAd(adType);
}

bool MTAppodealHelper::isReadyForShow(MTAppodeal::AdType adType) {
	return MTAppodealWrapper::isReadyForShow(adType);
}

// -------------------------------------
// MARK: General Methods - other

#else
#endif

// -------------------------------------
// MARK: Helpers

std::string MTAppodealHelper::adNameStringforType(MTAppodeal::AdType adType) {
	
	std::string adName = "banner";
	switch (adType) {
		case MTAppodeal::AdType::Banner:
			adName = "banner";
			break;
		case MTAppodeal::AdType::Interstitial:
			adName = "interstitial";
			break;
		case MTAppodeal::AdType::RewardedVideo:
			adName = "rewardedVideo";
			break;
			
		default:
			CCASSERT(0, "Unexpected adType");
			break;
	}
	
	return adName;
}

void MTAppodealHelper::callListenerForBannerState(MTAppodeal::AdState bannerState) {
	switch (bannerState) {
		case MTAppodeal::MTAppodealAdStateDidLoad:
			_listener->onBannerDidLoadAd();
			break;
		case MTAppodeal::MTAppodealAdStateFailedToLoad:
			_listener->onBannerDidFailToLoadAd();
			break;
		case MTAppodeal::MTAppodealAdStateDidShow:
			_listener->onBannerPresent();
			break;
		case MTAppodeal::MTAppodealAdStateDidClick:
			_listener->onBannerDidClick();
			break;
			
		default:
			break;
	}
}

void MTAppodealHelper::callListenerForInterstitialState(MTAppodeal::AdState interstitialState) {
	
	switch (interstitialState) {
		case MTAppodeal::MTAppodealAdStateDidLoad:
			_listener->onInterstitialDidLoadAd();
			break;
		case MTAppodeal::MTAppodealAdStateFailedToLoad:
			_listener->onInterstitialDidFailToLoadAd();
			break;
		case MTAppodeal::MTAppodealAdStateFailedToPresent:
			
			break;
		case MTAppodeal::MTAppodealAdStateWillPresent:
			_listener->onInterstitialWillPresent();
			break;
		case MTAppodeal::MTAppodealAdStateDidDismiss:
			_listener->onInterstitialDidDismiss();
			break;
		case MTAppodeal::MTAppodealAdStateDidClick:
			_listener->onInterstitialDidClick();
			break;
			
		default:
			break;
	}
}

void MTAppodealHelper::callListenerForRewardedState(MTAppodeal::AdState rewardedState) {
	switch (rewardedState) {
		case MTAppodeal::MTAppodealAdStateDidLoad:
			_listener->onRewardVideoDidLoadAd();
			break;
		case MTAppodeal::MTAppodealAdStateFailedToLoad:
			_listener->onRewardVideoDidFailToLoadAd();
			break;
		case MTAppodeal::MTAppodealAdStateFailedToPresent:
			
			break;
		case MTAppodeal::MTAppodealAdStateDidPresent:
			_listener->onRewardVideoDidPresent();
			break;
		case MTAppodeal::MTAppodealAdStateWillDismiss:
			_listener->onRewardVideoWillDismiss();
			break;
		case MTAppodeal::MTAppodealAdStateVideoDidFinish: {
			int amount = 0;
			std::string name = "rewardedVideo";
			_listener->onRewardVideoDidFinish(amount, name);
			break;
		}
			
		default:
			break;
	}
}
