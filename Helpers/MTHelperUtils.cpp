//
//  MTHelperUtils.cpp
//  QuizBattle
//
//  Created by Paul Gorbunov on 20.10.16.
//
//


#include "MTHelperUtils.h"

USING_NS_CC;

float mt_contentHeight(const cocos2d::Node* node) {
	return node->getContentSize().height;
}

float mt_contentWidth(const cocos2d::Node* node) {
	return node->getContentSize().width;
}

float mt_boundsHeight(const cocos2d::Node* node) {
	return node->getBoundingBox().size.height;
}

float mt_boundsWidth(const cocos2d::Node* node) {
	return node->getBoundingBox().size.width;
}

cocos2d::Vec2 mt_center(const cocos2d::Node* node) {
	return Vec2(mt_boundsWidth(node) * 0.5, mt_boundsHeight(node) * 0.5);
}

std::vector<uint8_t> mt_str_to_vector( const std::string& str ) {
	return std::vector<uint8_t>( str.begin(), str.end() );
}

std::string mt_vec_to_string( const std::vector<uint8_t>& v ) {
	return std::string(v.begin(), v.end());
}

bool mt_copyFile(const std::string fromPath, const std::string toPath) {
	auto data = FileUtils::getInstance()->getDataFromFile(fromPath);
	FILE* dest = fopen(toPath.c_str(), "wb");
	size_t dataSize = data.getSize();
	size_t elementsWritten = fwrite(data.getBytes(), sizeof(char), dataSize, dest);
	fclose(dest);
	
	return (elementsWritten == dataSize);
}

std::string mt_vectorDescription(const cocos2d::ValueVector& valueVector) {
	std::string description = "<";
	for (auto value : valueVector) {
		description += StringUtils::toString(value.asInt()) + ", ";
	}
	
	description += ">";
	return description;
}

// -------------------------------------
// MARK: Dictionary Methods

cocos2d::Value mt_getSafeValue(const cocos2d::ValueMap& valueMap, const std::string& key) {
	auto it = valueMap.find(key);
	if (it != valueMap.end()) {
		return valueMap.at(key);
	}
	
	return Value::Null;
}

bool mt_getSafeBoolValue(const cocos2d::ValueMap& valueMap, const std::string& key, bool defaultValue) {
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asBool();
	}
	
	return defaultValue;
}

int mt_getSafeIntValue(const cocos2d::ValueMap& valueMap, const std::string& key, int defaultValue) {
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asInt();
	}
	
	return defaultValue;
}

unsigned int mt_getSafeUIntValue(const cocos2d::ValueMap& valueMap, const std::string& key, unsigned int defaultValue) {
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asUnsignedInt();
	}
	
	return defaultValue;
}

float mt_getSafeFloatValue(const cocos2d::ValueMap& valueMap, const std::string& key, float defaultValue) {
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asFloat();
	}
	
	return defaultValue;
}

double mt_getSafeDoubleValue(const cocos2d::ValueMap& valueMap, const std::string& key, double defaultValue) {
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asDouble();
	}
	
	return defaultValue;
}

std::string mt_getSafeStringValue(const cocos2d::ValueMap& valueMap, const std::string& key, std::string defaultValue) {
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asString();
	}
	
	return defaultValue;
}

cocos2d::ValueVector mt_getSafeVectorValue(const cocos2d::ValueMap& valueMap, const std::string& key, cocos2d::ValueVector defaultValue) {
	
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asValueVector();
	}
	
	return defaultValue;
}

cocos2d::ValueMap mt_getSafeMapValue(const cocos2d::ValueMap& valueMap, const std::string& key, cocos2d::ValueMap defaultValue) {
	
	auto value = mt_getSafeValue(valueMap, key);
	if (value != Value::Null) {
		return value.asValueMap();
	}
	
	return defaultValue;
}
