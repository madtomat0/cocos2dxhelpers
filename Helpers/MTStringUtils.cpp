//
//  MTStringUtils.cpp
//  FillTheWords-mobile
//
//  Created by Pavel on 02/10/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTStringUtils.h"
#include "utf8.h"

USING_NS_CC;

namespace MTStringUtils
{
	
std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, std::back_inserter(elems));
	return elems;
}
	
std::string removeLastComponentFromPath(const std::string& originalPath) {
	auto pathComponents = split(originalPath, '/');
	std::string trimmedPath = "";
	for (int i = 0; i < pathComponents.size() - 1; i++) {
		std::string pathPart = pathComponents.at(i);
		trimmedPath = trimmedPath + pathPart + "/";
	}
	
	return trimmedPath;
}
	
bool stringContainsSubstring(const std::string& inputString, const std::string& substring) {
	
	return (inputString.find(substring) != std::string::npos);
}
	
std::string getPlatformString() {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	return "iOS";
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	return "Android";
#endif
}
	
std::string getCoronaWritablePath() {
	std::string coronaWritablePath = "";
	
	std::string cocosWritablePath = FileUtils::getInstance()->getWritablePath();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	coronaWritablePath = cocosWritablePath;
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	coronaWritablePath = MTStringUtils::removeLastComponentFromPath(cocosWritablePath);
	coronaWritablePath += "app_data/";
#endif
	
	return coronaWritablePath;
}
	
cocos2d::ValueVector createLettersVector(const std::string& originalString) {
	cocos2d::ValueVector lettersVector = ValueVectorNull;
	
	char* str = (char*)originalString.c_str();    // utf-8 string
	char* str_i = str;                  // string iterator
	char* end = str+strlen(str)+1;      // end iterator
	
	/*unsigned*/ char symbol[5] = {0,0,0,0,0};
	
	do
	{
		uint32_t code = utf8::next(str_i, end); // get 32 bit code of a utf-8 symbol
		if (code == 0)
			continue;
		
		utf8::append(code, symbol); // initialize array `symbol`
		std::string letString(symbol);
		lettersVector.push_back(Value(symbol));
	}
	while ( str_i < end );
	
	return lettersVector;
}
	
} // namespace MTStringUtils }
