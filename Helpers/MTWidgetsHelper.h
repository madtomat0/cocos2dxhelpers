//
//  MTWidgetsHelper.h
//  GoldDigger
//
//  Created by MAD Tomato on 15.02.15.
//
//

#ifndef __GoldDigger__MTWidgetsHelper__
#define __GoldDigger__MTWidgetsHelper__

#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>
#include "../Widgets/MTText.h"
#include "../Widgets/MTButton.h"
#include "../Widgets/MTCheckbox.h"

typedef std::function<void ()> BtnCallbackBlock;

class MTWidgetsHelper {
public:
	
	// Setup Methods
	
	static MTButton* setupBtn(const std::string& frameName, cocos2d::Vec2 pos, cocos2d::Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor = false);
	
	static MTButton* setupBtn(const std::string& frameName, const std::string& selectedFrameName, cocos2d::Vec2 pos, cocos2d::Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor = false);
	
	static MTCheckbox* setupCheckboxBtn(const std::string& frameName, const std::string& selectedFrameName, cocos2d::Vec2 pos, cocos2d::Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor = false);
	
	static MTText* setupLabelBtn(const std::string& btnText, const std::string& fontName, float fontSize, cocos2d::Vec2 pos, cocos2d::Node* parentNode, const BtnCallbackBlock& callback, bool useScaleFactor = false);
    
    static MTButton* setupLabelBtnWithBg(const std::string& btnText, const std::string& bgImageName, const cocos2d::Size& btnSize, const std::string& fontName, float fontSize, cocos2d::Vec2 pos, cocos2d::Node* parentNode, const BtnCallbackBlock& callbackbool, bool useScaleFactor = false);
	
	static cocos2d::ui::Layout* setupLayout(const cocos2d::Size& size, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, const cocos2d::Color3B color, bool useColor);
	
	static cocos2d::ui::Text* setupLabel(const std::string& textContent, const std::string& fontName, float fontSize, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, const cocos2d::Color3B color, bool useScaleFactor = false);
	
	static cocos2d::Sprite* setupSpriteRect(const cocos2d::Size& size, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, const cocos2d::Color3B color);
	
	static cocos2d::Sprite* setupSprite(const std::string& frameName, const cocos2d::Vec2& anchor, const cocos2d::Vec2& pos, cocos2d::Node* parentNode, bool useScaleFactor = false);
	
	// Utils
	
	static const cocos2d::Size getSpriteBounds(const std::string& spriteFrameName, bool useScaleFactor = false);
};

#endif /* defined(__GoldDigger__MTWidgetsHelper__) */
