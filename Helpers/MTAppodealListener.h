//
//  MTAppodealListener.h
//  FillTheWords
//
//  Created by Pavel on 18/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTAppodealListener_h
#define MTAppodealListener_h

namespace MTAppodeal {
	enum AdType {Banner = 0, Interstitial, RewardedVideo};
	enum AdState {MTAppodealAdStateDidLoad = 0,
		MTAppodealAdStateFailedToLoad,
		MTAppodealAdStateFailedToPresent,
		MTAppodealAdStateWillPresent,
		MTAppodealAdStateDidPresent,
		MTAppodealAdStateDidClick,
		MTAppodealAdStateDidShow,
		MTAppodealAdStateWillDismiss,
		MTAppodealAdStateDidDismiss,
		MTAppodealAdStateVideoDidFinish};
}

class MTAppodealListener {
public:
	virtual void onBannerDidLoadAd() {};
	virtual void onBannerDidFailToLoadAd() {};
	virtual void onBannerDidClick() {};
	virtual void onBannerPresent() {};
	
	virtual void onInterstitialDidLoadAd() {};
	virtual void onInterstitialDidFailToLoadAd() {};
	virtual void onInterstitialWillPresent() {};
	virtual void onInterstitialDidDismiss() {};
	virtual void onInterstitialDidClick() {};
	
	virtual void onRewardVideoDidLoadAd() {};
	virtual void onRewardVideoDidFailToLoadAd() {};
	virtual void onRewardVideoDidPresent() {};
	virtual void onRewardVideoWillDismiss() {};
	virtual void onRewardVideoDidFinish(int amount, const std::string& name) {};
};

#endif /* MTAppodealListener_h */
