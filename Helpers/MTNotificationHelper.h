//
//  MTNotificationHelper.hpp
//  FillTheWords-mobile
//
//  Created by Paul Gorbunov on 11/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTNotificationHelper_hpp
#define MTNotificationHelper_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "MTNotificationListener.h"

class MTNotificationHelper : public MTNotificationListener {
public:
	/** returns a shared instance */
	static MTNotificationHelper* getInstance();
	
	static const std::string notificationDidReceive;
	
	//typedef std::function<void (bool success)> SharingCallback;
	
	void prepareListener();
	
	void scheduleLocalNotification(float periodSeconds, const std::string& messageToShow, const std::string& notificationKey);
	
	bool hasNotification(const std::string& notificationKey) const;
	bool hasEmptyNotification() const;
	
	void clearAllNotifications();
	
	// MTNotificationListener
	virtual void localNotificationDidReceive(const std::string& notificationKey, const std::string& message);
	
private:
	const char* AppActivityClassName = "org/cocos2dx/cpp/AppActivity";
	
	bool init();
	MTNotificationHelper();
	~MTNotificationHelper();
	
	cocos2d::ValueMap _receivedNotifications;
};

#endif /* MTNotificationHelper_hpp */
