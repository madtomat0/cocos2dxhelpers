//
//  MTSharingListener.h
//  FillTheWords
//
//  Created by Pavel on 08/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTSharingListener_h
#define MTSharingListener_h

class MTSharingListener {
public:
	virtual void messageWasPostedWithSuccess(bool success) = 0;
};

#endif /* MTSharingListener_h */
