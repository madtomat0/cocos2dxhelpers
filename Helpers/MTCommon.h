//
//  MTCommon.h
//  LogicIntuition
//
//  Created by MAD Tomato on 17.03.15.
//
//

#ifndef LogicIntuition_MTCommon_h
#define LogicIntuition_MTCommon_h

enum class MTLanguageType
{
	ENGLISH = 0,
	CHINESE_SIMPLIFIED,
	CHINESE_TRADITIONAL,
	FRENCH,
	ITALIAN,
	GERMAN,
	SPANISH,
	DUTCH,
	RUSSIAN,
	KOREAN,
	JAPANESE,
	HUNGARIAN,
	PORTUGUESE,
	ARABIC,
	NORWEGIAN,
	POLISH,
	TURKISH,
	UKRAINIAN
};

#endif
