//
//  MTHelperUtils.h
//  GoldDigger
//
//  Created by MAD Tomato on 02.03.15.
//
//

#ifndef GoldDigger_MTHelperUtils_h
#define GoldDigger_MTHelperUtils_h

#include <stdio.h>
#include <sstream>
#include <vector>
#include "cocos2d.h"

typedef std::function<void (bool success, const std::string errorDescription)> MTOperationCallback;

float mt_contentHeight(const cocos2d::Node* node);

float mt_contentWidth(const cocos2d::Node* node);

float mt_boundsHeight(const cocos2d::Node* node);

float mt_boundsWidth(const cocos2d::Node* node);

cocos2d::Vec2 mt_center(const cocos2d::Node* node);

std::vector<uint8_t> mt_str_to_vector( const std::string& str );

std::string mt_vec_to_string( const std::vector<uint8_t>& v );

bool mt_copyFile(const std::string fromPath, const std::string toPath);

template <typename T>
cocos2d::Vector<T> mt_shuffledVector(cocos2d::Vector<T> initialVector) {
    ssize_t qty = initialVector.size();
    auto randomizedVector = cocos2d::Vector<T>();
    for (ssize_t i = 0; i < qty; i++) {
        auto randObject = initialVector.getRandomObject();
        randomizedVector.pushBack(randObject);
        initialVector.eraseObject(randObject);
    }
    
    return randomizedVector;
}

template <typename T>
std::vector<T> mt_shuffledVector(std::vector<T> initialVector) {
	ssize_t initialQty = initialVector.size();
	std::vector<std::string> randomizedVector;
	for (ssize_t i = 0; i < initialQty; i++) {
		ssize_t qty = initialVector.size();
		ssize_t randIndex = cocos2d::random<ssize_t>(0, qty-1);
		auto randObject = initialVector.at(randIndex);
		randomizedVector.push_back(randObject);
		
		auto it = std::find(initialVector.begin(), initialVector.end(), randObject);
		if(it != initialVector.end()) initialVector.erase(it);
	}
	
	return randomizedVector;
}

std::string mt_vectorDescription(const cocos2d::ValueVector& valueVector);

// Dictionary methods

cocos2d::Value mt_getSafeValue(const cocos2d::ValueMap& valueMap, const std::string& key);

bool mt_getSafeBoolValue(const cocos2d::ValueMap& valueMap, const std::string& key, bool defaultValue = false);

int mt_getSafeIntValue(const cocos2d::ValueMap& valueMap, const std::string& key, int defaultValue = 0);

unsigned int mt_getSafeUIntValue(const cocos2d::ValueMap& valueMap, const std::string& key, unsigned int defaultValue = 0);

float mt_getSafeFloatValue(const cocos2d::ValueMap& valueMap, const std::string& key, float defaultValue = 0.0);

double mt_getSafeDoubleValue(const cocos2d::ValueMap& valueMap, const std::string& key, double defaultValue = 0.0);

std::string mt_getSafeStringValue(const cocos2d::ValueMap& valueMap, const std::string& key, std::string defaultValue = "");

cocos2d::ValueVector mt_getSafeVectorValue(const cocos2d::ValueMap& valueMap, const std::string& key, cocos2d::ValueVector defaultValue = cocos2d::ValueVectorNull);

cocos2d::ValueMap mt_getSafeMapValue(const cocos2d::ValueMap& valueMap, const std::string& key, cocos2d::ValueMap defaultValue = cocos2d::ValueMapNull);

#endif
