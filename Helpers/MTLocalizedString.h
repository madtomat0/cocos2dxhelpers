//
//  MTLocalizedString.h
//  Xonix
//
//  Created by MAD Tomato on 15.12.14.
//
//

#ifndef __Xonix__MTLocalizedString__
#define __Xonix__MTLocalizedString__

#include <stdio.h>
#include "cocos2d.h"

class MTLocalizedString {
public:
	static std::string getLocalizedString(const std::string& keyString);
	
	static void enableForcedLanguageType(bool enable, cocos2d::LanguageType forcedType);
    
    static std::string getCurrentLanguage();
    static cocos2d::LanguageType getCurrentLanguageType();
private:
	
	static bool _forcedLanguegeEnabled;
	static cocos2d::LanguageType _forcedType;
};

#endif /* defined(__Xonix__MTLocalizedString__) */
