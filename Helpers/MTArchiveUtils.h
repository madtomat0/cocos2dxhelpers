//
//  MTArchiveUtils.hpp
//  WordTiles
//
//  Created by Pavel on 23/08/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTArchiveUtils_hpp
#define MTArchiveUtils_hpp

#include <stdio.h>
#include "cocos2d.h"

class MTArchiveUtils {
public:
	
	typedef std::function<void (bool success)> UnarchiveCallback;
	
	static void uncompressFile(const std::string& pathToFile, const UnarchiveCallback& callback);
	
private:
	static bool uncompress(const std::string& pathToFile);
};

#endif /* MTArchiveUtils_hpp */
