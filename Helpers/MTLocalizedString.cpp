//
//  MTLocalizedString.cpp
//  Xonix
//
//  Created by MAD Tomato on 15.12.14.
//
//

#include "MTLocalizedString.h"

USING_NS_CC;
using namespace std;

// -------------------------------------
// MARK: General Methods

static ValueMap _stringsMap = ValueMapNull;

string MTLocalizedString::getLocalizedString(const string &keyString) {
	
	if (_stringsMap == ValueMapNull) {
		std::string language = MTLocalizedString::getCurrentLanguage();
		std::string locFileName = StringUtils::format("%s_strings.plist", language.c_str());
		_stringsMap = FileUtils::getInstance()->getValueMapFromFile(locFileName);
	}
	
	if (_stringsMap != ValueMapNull) {
		if (_stringsMap.find(keyString) != _stringsMap.end()) {
			string stringValue = _stringsMap.at(keyString).asString();
			return stringValue;
		}
	}
	
	return keyString;
}

bool MTLocalizedString::_forcedLanguegeEnabled = false;
cocos2d::LanguageType MTLocalizedString::_forcedType = LanguageType::ENGLISH;

void MTLocalizedString::enableForcedLanguageType(bool enable, cocos2d::LanguageType forcedType) {
	
	_forcedType = forcedType;
	_forcedLanguegeEnabled = enable;
}

// -------------------------------------
// MARK: Helpers

cocos2d::LanguageType MTLocalizedString::getCurrentLanguageType() {
	if (_forcedLanguegeEnabled) {
		return _forcedType;
	} else {
		return Application::getInstance()->getCurrentLanguage();
	}
}

string MTLocalizedString::getCurrentLanguage() {
	LanguageType langType = MTLocalizedString::getCurrentLanguageType();
	
	CCLOG("Defining language..");
	CCLOG("Lang type = %d", static_cast<int>(langType));
	std::string lang = "en";
	
	switch (langType) {
		case LanguageType::GERMAN:
			lang = "de";
			break;
		case LanguageType::ENGLISH:
			lang = "en";
			break;
		case LanguageType::SPANISH:
			lang = "es";
			break;
		case LanguageType::FRENCH:
			lang = "fr";
			break;
		case LanguageType::ITALIAN:
			lang = "it";
			break;
		case LanguageType::JAPANESE:
			lang = "ja";
			break;
		case LanguageType::KOREAN:
			lang = "ko";
			break;
		case LanguageType::DUTCH:
			lang = "nl";
			break;
		case LanguageType::PORTUGUESE:
			lang = "pt";
			break;
		case LanguageType::RUSSIAN:
			lang = "ru";
			break;
		case LanguageType::TURKISH:
			lang = "tr";
			break;
			
		default:
			lang = "en";
			break;
	}
	
	CCLOG("Chosen lang = %s", lang.c_str());
	
	return lang;
}
