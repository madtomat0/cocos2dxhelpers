//
//  MTPlatformHelper.hpp
//  WordTiles
//
//  Created by Pavel on 03/07/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTPlatformHelper_hpp
#define MTPlatformHelper_hpp

#include <stdio.h>
#include "cocos2d.h"

class MTPlatformHelper {
public:
	
	static std::string getCurrentOSVersion();
	static std::string getDeviceModel();
	static bool isAndroidOS();
	static bool isiPhoneX();
	static bool isPortraitScreenOrientation();
	
};

#endif /* MTPlatformHelper_hpp */
