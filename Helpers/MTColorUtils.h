//
//  MTColorUtils.hpp
//  FillTheWords-mobile
//
//  Created by Pavel on 27/09/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTColorUtils_hpp
#define MTColorUtils_hpp

#include <stdio.h>
#include "cocos2d.h"

namespace MTColorUtils
{
	cocos2d::Color3B colorFromHexString(const std::string& hexString);
}

#endif /* MTColorUtils_hpp */
