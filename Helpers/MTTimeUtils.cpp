//
//  MTTimeUtils.cpp
//  FillTheWords-mobile
//
//  Created by Pavel on 04/12/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTTimeUtils.h"
#include "MTLocalizedString.h"

USING_NS_CC;
using namespace std;
using namespace std::chrono;
using std::chrono::steady_clock;
using std::chrono::system_clock;

namespace MTTimeUtils
{
	double timePointToSeconds(const std::chrono::steady_clock::time_point& timePoint) {
		auto timePointSeconds = time_point_cast<seconds>(timePoint);
		auto value = timePointSeconds.time_since_epoch();
		double duration = (double)value.count();
		
		return duration;
	}
	
	double systemTimePointToSeconds(const std::chrono::system_clock::time_point& timePoint) {
		
		auto timePointSeconds = time_point_cast<seconds>(timePoint);
		auto value = timePointSeconds.time_since_epoch();
		double duration = (double)value.count();
		
		return duration;
	}
	
	steady_clock::time_point timePointFromSeconds(double seconds) {
		std::chrono::seconds dur((long)seconds);
		time_point<std::chrono::steady_clock> dt(dur);
		
		return dt;
	}
	
	std::chrono::system_clock::time_point systemTimePointFromSeconds(double seconds) {
		std::chrono::seconds dur((long)seconds);
		time_point<std::chrono::system_clock> dt(dur);
		
		return dt;
	}
	
	std::string formattedTime(const steady_clock::time_point& timePoint) {
		
		auto duration = duration_cast<system_clock::duration>(timePoint - steady_clock::now());
		
		time_t systemClockTime = system_clock::to_time_t(system_clock::now() + duration);
		
		const char* timeFormat = (MTLocalizedString::getCurrentLanguageType() == LanguageType::RUSSIAN) ? "%d-%m-%Y %H:%M" : "%m-%d-%Y %I:%M %p";
		
		char buff[20];
		strftime(buff, 20, timeFormat, localtime(&systemClockTime));
		
		return std::string(buff);
	}
	
	std::string systemFormattedTime(const std::chrono::system_clock::time_point& timePoint) {
		
		time_t systemClockTime = system_clock::to_time_t(timePoint);
		
		const char* timeFormat = (MTLocalizedString::getCurrentLanguageType() == LanguageType::RUSSIAN) ? "%d-%m-%Y %H:%M" : "%m-%d-%Y %I:%M %p";
		
		char buff[20];
		strftime(buff, 20, timeFormat, localtime(&systemClockTime));
		
		return std::string(buff);
	}
	
	std::string formattedTime(double seconds) {
		auto timePoint = MTTimeUtils::timePointFromSeconds(seconds);
		return MTTimeUtils::formattedTime(timePoint);
	}
	
	std::string systemFormattedTime(double seconds) {
		auto timePoint = MTTimeUtils::systemTimePointFromSeconds(seconds);
		return MTTimeUtils::systemFormattedTime(timePoint);
	}
	
} // namespace MTTimeUtils
