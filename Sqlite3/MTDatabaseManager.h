//
//  MTDatabaseManager.h
//  QuizBattle
//
//  Created by Paul Gorbunov on 25.10.16.
//
//

#ifndef MTDatabaseManager_hpp
#define MTDatabaseManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <external/sqlite3/include/sqlite3.h>
#include "../../MTDefines.h"

class MTDatabaseManager : public cocos2d::Ref {
public:
	
	typedef std::function<void (bool success)> DBCopyCallback;
	
	/** returns a shared instance */
	static MTDatabaseManager* getInstance();
	~MTDatabaseManager();
	
	void setDatabaseFileName(const std::string& databaseName);
	
	bool open();
	bool openAtPath(const std::string& fullDBPath);
	void close();
	
	bool execute(std::string& aSql);
	
	/**
	 Copies db from Resources to writable directory asynchronously
	 */
	void copyDBWithCallbackBlock(const DBCopyCallback& callbackBlock);
	
	/**
	 Executes JOIN type sql query synchronously. Combines rows from two tables and returns data from joined table as vector in main object's dictionary
	 */
	cocos2d::ValueMap getJoinedDataAsValueMap(const std::string& queryString, const char* joinedTableName, MTError** error);
	
    /**
     Executes simple sql query synchronously. Returns single object data as dictionary
     */
    cocos2d::ValueMap getDataAsValueMap(const std::string& queryString, MTError** error);
    
    /**
     Executes simple sql query synchronously. Returns objects data as vector of dictionaries
     */
	cocos2d::ValueVector getDataAsValueVector(const std::string& queryString, MTError** error);
	
private:
	bool init();
	MTDatabaseManager();
	
	/**
	 Returns true if DB was successfully copied
	 */
	bool copyDBFromResToWritablePath();
	
	sqlite3_stmt* createQueryStatement(const std::string& queryString, MTError** error);
	static cocos2d::Value getColValue(sqlite3_stmt* statement, const char* colType, int col);
	
	sqlite3* _database;
	std::mutex _copyDBMutex;
	std::string _databaseFileName;
};

#endif /* MTDatabaseManager_hpp */
