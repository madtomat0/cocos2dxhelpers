//
//  MTDatabaseManager.cpp
//  QuizBattle
//
//  Created by Paul Gorbunov on 25.10.16.
//
//

#include "MTDatabaseManager.h"
#include "../../MTDefines.h"
#include <thread>

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTDatabaseManager::MTDatabaseManager()
: _database(nullptr)
, _databaseFileName("")
{
	
}

MTDatabaseManager::~MTDatabaseManager() {
	
	CCLOG("~MTDatabaseManager deleted");
}

// -------------------------------------
// MARK: Initialization

// singleton stuff
static MTDatabaseManager* s_sharedManager = nullptr;

MTDatabaseManager* MTDatabaseManager::getInstance() {
	if (!s_sharedManager) {
		s_sharedManager = new (std::nothrow) MTDatabaseManager();
		CCASSERT(s_sharedManager, "FATAL: Not enough memory");
		s_sharedManager->init();
	}
	
	return s_sharedManager;
}

bool MTDatabaseManager::init() {
	CCLOG("MTDatabaseManager init started");
	
	CCLOG("MTDatabaseManager init finished");
	
	return true;
}

void MTDatabaseManager::setDatabaseFileName(const std::string& databaseName) {
	_databaseFileName = databaseName;
}

bool MTDatabaseManager::open() {
	std::string filePath = FileUtils::getInstance()->getWritablePath() + _databaseFileName;
	return this->openAtPath(filePath);
}

bool MTDatabaseManager::openAtPath(const std::string& fullDBPath) {
	if (sqlite3_open(fullDBPath.c_str(), &_database) != SQLITE_OK) {
		sqlite3_close(_database);
		CCLOG("Failed to open database: %s", fullDBPath.c_str());
		return false;
	}
	
	return true;
}

void MTDatabaseManager::close() {
	sqlite3_close(_database);
	_database = nullptr;
}

bool MTDatabaseManager::execute(std::string& aSql) {
	char* errorMsg;
	
	if (sqlite3_exec(_database, aSql.c_str(), NULL, NULL, &errorMsg) != SQLITE_OK) {
		std::string status = StringUtils::format("Error executing SQL statement: %s", errorMsg);
		CCLOG("%s", status.c_str());
		return false;
	}
	
	return true;
}

// -------------------------------------
// MARK: General Methods

void MTDatabaseManager::copyDBWithCallbackBlock(const DBCopyCallback& callbackBlock) {
	std::thread thread = std::thread([this, callbackBlock](){
		bool copySuccess = this->copyDBFromResToWritablePath();
		if (callbackBlock) {
			Director::getInstance()->getScheduler()->performFunctionInCocosThread([callbackBlock, copySuccess](){
				callbackBlock(copySuccess);
			});
		}
	});
	
	thread.join();
}

cocos2d::ValueMap MTDatabaseManager::getJoinedDataAsValueMap(const std::string& queryString, const char* joinedTableName, MTError** error) {
	
	sqlite3_stmt* queryStatement = this->createQueryStatement(queryString, error);
	
	if (*error) {
		return ValueMapNull;
	}
	
	ValueMap mainDataDict = ValueMapNull;
	ValueVector childDataVector = ValueVectorNull;
	int columnsQty = sqlite3_column_count(queryStatement);
	
	while (sqlite3_step(queryStatement) == SQLITE_ROW) {
		
		ValueMap childDataDict = ValueMapNull;
		
		for (int col = 0; col < columnsQty; col ++) {
			auto colType = sqlite3_column_decltype(queryStatement, col);
			auto colName = sqlite3_column_name(queryStatement, col);
			auto colTableName = sqlite3_column_table_name(queryStatement, col);
			
			Value colValue = this->getColValue(queryStatement, colType, col);
			
			if (strcmp(colTableName, joinedTableName) == 0) {
				childDataDict[colName] = colValue;
			} else {
				mainDataDict[colName] = colValue;
			}
		}
		
		childDataVector.push_back(Value(childDataDict));
		childDataDict = ValueMapNull;
	}
	
	if (childDataVector != ValueVectorNull) {
		mainDataDict[joinedTableName] = childDataVector;
	}
	
	sqlite3_finalize(queryStatement);
	
	return mainDataDict;
}

cocos2d::ValueMap MTDatabaseManager::getDataAsValueMap(const std::string& queryString, MTError** error) {
	auto dataValueVector = this->getDataAsValueVector(queryString, error);
	if (*error == nullptr && dataValueVector != ValueVectorNull) {
		return dataValueVector.at(0).asValueMap();
	}
	
	return ValueMapNull;
}

cocos2d::ValueVector MTDatabaseManager::getDataAsValueVector(const std::string& queryString, MTError** error) {
	
	sqlite3_stmt* queryStatement = this->createQueryStatement(queryString, error);
	
	if (*error) {
		return ValueVectorNull;
	}
	
	ValueVector objectsValueVector = ValueVectorNull;
	int columnsQty = sqlite3_column_count(queryStatement);
	
	while (sqlite3_step(queryStatement) == SQLITE_ROW) {
		
		ValueMap mainDataDict = ValueMapNull;
		
		for (int col = 0; col < columnsQty; col ++) {
			auto colType = sqlite3_column_decltype(queryStatement, col);
			auto colName = sqlite3_column_name(queryStatement, col);
			
			Value colValue = this->getColValue(queryStatement, colType, col);
			
			mainDataDict[colName] = colValue;
		}
		
		if (mainDataDict != ValueMapNull) {
			objectsValueVector.push_back(Value(mainDataDict));
		}
	}
	
	sqlite3_finalize(queryStatement);
	
	return objectsValueVector;
}

// -------------------------------------
// MARK: Helpers

bool MTDatabaseManager::copyDBFromResToWritablePath() {
	std::lock_guard<std::mutex> guard(_copyDBMutex);
	
	std::string resorcesFilePath = FileUtils::getInstance()->fullPathForFilename(_databaseFileName);
	CCLOG("resorcesFilePath: %s", resorcesFilePath.c_str());
	
	std::string writableFilePath = FileUtils::getInstance()->getWritablePath() + _databaseFileName;
	CCLOG("writableFilePath: %s", writableFilePath.c_str());
	
	bool copySuccess = false;
	if (FileUtils::getInstance()->isFileExist(writableFilePath)) {
		CCLOG("DB already exists. Will overwrite it with the one from resources");
		copySuccess = mt_copyFile(resorcesFilePath, writableFilePath);
	} else {
		CCLOG("Will copy DB from resources to writable path");
		copySuccess = mt_copyFile(resorcesFilePath, writableFilePath);
	}
	
	return copySuccess;
}

sqlite3_stmt* MTDatabaseManager::createQueryStatement(const std::string& queryString, MTError** error) {
	
	sqlite3_stmt* queryStatement = nullptr;
	int errCode = sqlite3_prepare_v2(_database, queryString.c_str(), -1, &queryStatement, nullptr);
	if (errCode != SQLITE_OK) {
		sqlite3_finalize(queryStatement);
		
		auto descString = StringUtils::format("Sqlte3 Prepare error: %d of SELECT statement could not be prepared. Statement: %s", errCode, queryString.c_str());
		CCLOG("%s", descString.c_str());
		*error = MTError::create(descString);
	}
	
	return queryStatement;
}

cocos2d::Value MTDatabaseManager::getColValue(sqlite3_stmt* statement, const char* colType, int col) {
	Value colValue;
	
	if (colType == NULL) {
		colValue = Value(sqlite3_column_int(statement, col));
		return colValue;
	}
	
	if (strcmp(colType, "INTEGER") == 0 ||
		strcmp(colType, "INT") == 0 ||
		strcmp(colType, "NUMERIC") == 0) {
		colValue = Value(sqlite3_column_int(statement, col));
	} else if (strcmp(colType, "TEXT") == 0 ||
			   strstr(colType, "VARCHAR") != NULL) {
		auto textString = StringUtils::format("%s", sqlite3_column_text(statement, col));
		textString = (textString == "(null)") ? "" : textString;
		colValue = Value(textString);
	} else if (strcmp(colType, "DOUBLE") == 0) {
		colValue = Value(sqlite3_column_double(statement, col));
	}
	
	return colValue;
}

// Test queries

// SELECT * FROM questions WHERE id IN (SELECT id FROM questions WHERE (category = 2 OR category = 1) AND id != 1 AND id !=2 AND id != 3 AND id !=4 AND id != 5 AND id !=14 AND id != 10 AND id !=7 ORDER BY RANDOM() LIMIT 50 )


