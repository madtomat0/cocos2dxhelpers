//
//  MTAdsManager.cpp
//  WordTiles
//
//  Created by Pavel on 10/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTAdsManager.h"
#include "../Helpers/MTScreenHelper.h"
#include "../../Model/Managers/GameDataManager.h"
#include "../Helpers/MTPlatformHelper.h"
#include "../Helpers/MTAdsHelper.h"

USING_NS_CC;

const std::string AdsEvent::bannerDidReceived = "AdsEvent_bannerDidReceived";
const std::string AdsEvent::fullScreenAdWillShow = "AdsEvent_fullScreenAdWillShow";
const std::string AdsEvent::fullScreenAdDidHide = "AdsEvent_fullScreenAdDidHide";

// -------------------------------------
// MARK: Ctors

MTAdsManager::MTAdsManager()
: _bannerWasCached(false)
, _bannerIsHidden(false)
, _interstitialCount(0)
, _interstitialCallback(nullptr)
, _rewardedCallback(nullptr)
, _bannerAdIsPreloading(false)
, _interstitialAdIsPreloading(false)
, _rewardedAdIsPreloading(false)
, _interstitialIsShown(false)
, _rewardedIsShown(false)
, _settingsProvider(nullptr)
, _interstitialThreshold(0)

// Const
, c_interstitialCountKey("FTW_interstitialCountKey")
{
	
}

MTAdsManager::~MTAdsManager() {
#if (USE_APPODEAL == 1)
	
#else
	sdkbox::PluginAdMob::removeListener();
#endif
	
	_interstitialCallback = nullptr;
	_rewardedCallback = nullptr;
	
	CCLOG("~MTAdsManager deleted");
}

// -------------------------------------
// MARK: Initialization

MT_SINGLETON_IMPL(MTAdsManager)

void MTAdsManager::setup(IAppSettingsProvider* settingsProvider, int interstitialThreshold) {
	_settingsProvider = settingsProvider;
	_interstitialThreshold = interstitialThreshold;
	
#if (USE_APPODEAL == 1)
//	sdkbox::PluginAppodeal::init();
//	sdkbox::PluginAppodeal::setListener(this);
//	sdkbox::PluginAppodeal::setSmartBannersEnabled(true);
//
//	sdkbox::PluginAppodeal::setAutocache(false, sdkbox::PluginAppodeal::AppodealAdTypeBanner);
//	sdkbox::PluginAppodeal::setAutocache(false, sdkbox::PluginAppodeal::AppodealAdTypeInterstitial);
//	sdkbox::PluginAppodeal::setAutocache(false, sdkbox::PluginAppodeal::AppodealAdTypeRewardVideo);
	
	std::string appodealAPIKey = (MTPlatformHelper::isAndroidOS()) ? "appodealApiKey_android" : "appodealApiKey_ios";
	MTAppodealHelper::getInstance()->setup(_settingsProvider->getConfigSetting(appodealAPIKey));
	MTAppodealHelper::getInstance()->setListener(this);
	
	this->tryToPreloadRewardedAd();
#else
	sdkbox::PluginAdMob::init();
	sdkbox::PluginAdMob::setListener(this);
	
	// Caching is not needed when using sdkbox. Must do it for custom though
	if (!MTPlatformHelper::isAndroidOS() && !_settingsProvider->adsDisabled()) {
		MTAdsHelper::getInstance()->cacheBanner();
	}
	
	// Sdkbox preloads ads on start
	_interstitialAdIsPreloading = true;
	_rewardedAdIsPreloading = true;
#endif
	
	_interstitialCount = UserDefault::getInstance()->getIntegerForKey(c_interstitialCountKey, 0);
}

// -------------------------------------
// MARK: Accessors

void MTAdsManager::setInterstitialCount(int value) {
	if (_interstitialCount != value) {
		_interstitialCount = value;
		UserDefault::getInstance()->setIntegerForKey(c_interstitialCountKey, _interstitialCount);
	}
}

// -------------------------------------
// MARK: General Methods

void MTAdsManager::showAd(AdType adType) {
	if (_settingsProvider->adsDisabled()) {
		return;
	}
	
	if (adType == Banner) {
		_bannerIsHidden = false;
	}
	
#if (USE_APPODEAL == 1)
	MTAppodealHelper::getInstance()->showAd(this->appodealAdTypeForAdType(adType));
#else
	if (adType == Banner && !MTPlatformHelper::isAndroidOS()) {
		MTAdsHelper::getInstance()->showBanner(0);// 0 is ignored for iOS
	} else {
		sdkbox::PluginAdMob::show(MTAdsManager::adNameForType(adType));
	}
#endif
}

void MTAdsManager::hideAd() {
	if (_settingsProvider->adsDisabled()) {
		return;
	}
	
	_bannerIsHidden = true;
	
#if (USE_APPODEAL == 1)
	MTAppodealHelper::getInstance()->hideBanner();
#else
	// Can hide only banner type ad
	if (MTPlatformHelper::isAndroidOS()) {
		sdkbox::PluginAdMob::hide(MTAdsManager::adNameForType(Banner));
	} else {
		MTAdsHelper::getInstance()->hideBanner();
	}
#endif
}

void MTAdsManager::disableAds(bool disable) {
	if (disable) {
		this->hideAd();
	}
	
	_settingsProvider->setAdsDisabled(disable);
}

float MTAdsManager::getBannerHeight() {
	if (_settingsProvider->adsDisabled()) {
		return 0.0;
	}
	
	if (MTPlatformHelper::isAndroidOS()) {
		if (!_bannerWasCached) {
			return 0.0;
		}
	}
	
	if (!this->adIsAvailable(Banner)) {
		return 0.0;
	}
	
	float heightPx = 0;
	
#if (USE_APPODEAL == 1)
	// appodeal has no method for retrieving banner height
	// will use 60px always
	heightPx = 60.0;
	
#else
	if (MTPlatformHelper::isAndroidOS()) {
		heightPx = float(sdkbox::PluginAdMob::getCurrBannerHeightInPixel());
	} else {
		heightPx = MTAdsHelper::getInstance()->getBannerHeight();
	}
#endif
	
	float height = MTScreenHelper::heightPixelsToHeight(heightPx);
	
	return MAX(0.0, height);
}

void MTAdsManager::tryToPreloadInterstitialAd() {
	if (this->adIsAvailable(Interstitial) || _interstitialAdIsPreloading) {
		return;
	}
	
	CCLOG("Loading interstitial ad");
	
	_interstitialAdIsPreloading = true;
	
#if (USE_APPODEAL == 1)
	MTAppodealHelper::getInstance()->cacheAd(MTAppodeal::Interstitial);
#else
	sdkbox::PluginAdMob::cache(MTAdsManager::adNameForType(Interstitial));
#endif
	
}

void MTAdsManager::tryToShowInterstitialAd(const AdShowCallback& callback) {
	if (_settingsProvider->adsDisabled()) {
		callback(false);
		return;
	}
	
	if (!this->adIsAvailable(Interstitial)) {
		CCLOG("Interstitial ad is not available. Will try to load it");
		this->tryToPreloadInterstitialAd();
		this->setInterstitialCount(_interstitialCount + 1);
		callback(false);
		return;
	}
	
	this->setInterstitialCount(_interstitialCount + 1);
	if (_interstitialCount >= _interstitialThreshold) {
		this->setInterstitialCount(0);
		
		if (!_bannerIsHidden) {
			this->hideAd();
		}
		
		_interstitialCallback = callback;
		this->showAd(Interstitial);
	} else {
		callback(false);
		_interstitialCallback = nullptr;
	}
}

void MTAdsManager::tryToPreloadRewardedAd() {
	if (this->adIsAvailable(RewardedVideo) || _rewardedAdIsPreloading) {
		return;
	}
	
	CCLOG("Loading rewarded ad");
	
	_rewardedAdIsPreloading = true;
	
#if (USE_APPODEAL == 1)
	MTAppodealHelper::getInstance()->cacheAd(MTAppodeal::RewardedVideo);
#else
	sdkbox::PluginAdMob::cache(MTAdsManager::adNameForType(RewardedVideo));
#endif
	
}

void MTAdsManager::showRewardedAd(const AdShowCallback& callback) {
	if (!this->adIsAvailable(RewardedVideo)) {
		this->tryToPreloadRewardedAd();
		callback(false);
		return;
	}
	
	_rewardedCallback = callback;
	
#if (USE_APPODEAL == 1)
	MTAppodealHelper::getInstance()->showAd(MTAppodeal::AdType::RewardedVideo);
#else
	sdkbox::PluginAdMob::show(MTAdsManager::adNameForType(RewardedVideo));
#endif
	
}

bool MTAdsManager::adIsAvailable(AdType adType) {
	
#if (USE_APPODEAL == 1)
	return MTAppodealHelper::getInstance()->isReadyForShow(this->appodealAdTypeForAdType(adType));
#else
	if (adType == Banner && !MTPlatformHelper::isAndroidOS()) {
		return (MTAdsHelper::getInstance()->getBannerHeight() > 0);
	} else {
		return sdkbox::PluginAdMob::isAvailable(MTAdsManager::adNameForType(adType));
	}
#endif
}

// -------------------------------------
// MARK: AdMobListener

void MTAdsManager::adViewDidReceiveAd(const std::string &name) {
	this->onAdLoaded(MTAdsManager::adTypeForName(name));
}

void MTAdsManager::adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg) {
	CCLOG("Failed to receive ad: %s. Message: %s", name.c_str(), msg.c_str());
	this->onAdFailedToLoad(MTAdsManager::adTypeForName(name));
}

void MTAdsManager::adViewWillPresentScreen(const std::string &name) {
	this->onAdWillPresentScreen(MTAdsManager::adTypeForName(name));
}

void MTAdsManager::adViewDidDismissScreen(const std::string &name) {
	this->onAdDismiss(MTAdsManager::adTypeForName(name));
}

void MTAdsManager::adViewWillDismissScreen(const std::string &name) {
	this->onAdDismiss(MTAdsManager::adTypeForName(name));
}

void MTAdsManager::adViewWillLeaveApplication(const std::string &name) {
	CCLOG("WillLeaveApp: %s", name.c_str());
}

void MTAdsManager::reward(const std::string &name, const std::string &currency, double amount) {
	this->onAdReward();
}

// -------------------------------------
// MARK: AppodealListener

// Banner
void MTAdsManager::onBannerDidLoadAd() {
	this->onAdLoaded(Banner);
}

void MTAdsManager::onBannerDidFailToLoadAd() {
	this->onAdFailedToLoad(Banner);
}

void MTAdsManager::onBannerDidClick() {
}

void MTAdsManager::onBannerPresent() {
	this->onAdWillPresentScreen(Banner);
}

// Interstitial
void MTAdsManager::onInterstitialDidLoadAd() {
	this->onAdLoaded(Interstitial);
}

void MTAdsManager::onInterstitialDidFailToLoadAd() {
	this->onAdFailedToLoad(Interstitial);
}

void MTAdsManager::onInterstitialWillPresent() {
	this->onAdWillPresentScreen(Interstitial);
}

void MTAdsManager::onInterstitialDidDismiss() {
	this->onAdDismiss(Interstitial);
}

void MTAdsManager::onInterstitialDidClick() {
}

// Rewarded Video
void MTAdsManager::onRewardVideoDidLoadAd() {
	this->onAdLoaded(RewardedVideo);
}

void MTAdsManager::onRewardVideoDidFailToLoadAd() {
	this->onAdFailedToLoad(RewardedVideo);
}

void MTAdsManager::onRewardVideoDidPresent() {
	this->onAdWillPresentScreen(RewardedVideo);
}

void MTAdsManager::onRewardVideoWillDismiss() {
	this->onAdDismiss(RewardedVideo);
}

void MTAdsManager::onRewardVideoDidFinish(int amount, const std::string& name) {
	this->onAdReward();
}

// -------------------------------------
// MARK: Helpers

void MTAdsManager::onAdLoaded(AdType adType) {
	CCLOG("Ad loaded: %s", MTAdsManager::adNameForType(adType).c_str());
	
	if (adType == Banner) {
		_bannerWasCached = true;
		
		auto dispatcher = Director::getInstance()->getEventDispatcher();
		dispatcher->dispatchCustomEvent(AdsEvent::bannerDidReceived);
		
	} else if (adType == Interstitial) {
		_interstitialAdIsPreloading = false;
	} else if (adType == RewardedVideo) {
		_rewardedAdIsPreloading = false;
	}
}

void MTAdsManager::onAdFailedToLoad(AdType adType) {
	CCLOG("Ad failed to load: %s", MTAdsManager::adNameForType(adType).c_str());
	
	if (adType == Banner) {
		_bannerWasCached = false;
	} else if (adType == Interstitial) {
		_interstitialAdIsPreloading = false;
	} else if (adType == RewardedVideo) {
		_rewardedAdIsPreloading = false;
	}
}

void MTAdsManager::onAdWillPresentScreen(AdType adType) {
	CCLOG("Ad will present screen: %s", MTAdsManager::adNameForType(adType).c_str());
	
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	
	if (adType == Banner) {
		// nothing to do here
	} else if (adType == Interstitial) {
		_interstitialIsShown = true;
		dispatcher->dispatchCustomEvent(AdsEvent::fullScreenAdWillShow);
	} else if (adType == RewardedVideo) {
		_rewardedIsShown = true;
		dispatcher->dispatchCustomEvent(AdsEvent::fullScreenAdWillShow);
	}
}

void MTAdsManager::onAdDismiss(AdType adType) {
	// For some reason callback adViewWillDismissScreen is only called on iOS
	// and callback adViewDidDismissScreen only on android.
	// So this method is called from both of the above callbacks
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	
	if (adType == Banner) {
		// nothing to do here
	} else if (adType == Interstitial) {
		if (_interstitialIsShown) {
			CCLOG("Ad dismiss screen: %s", MTAdsManager::adNameForType(adType).c_str());
			_interstitialIsShown = false;
			if (_interstitialCallback) _interstitialCallback(true);
			_interstitialCallback = nullptr;
			
			dispatcher->dispatchCustomEvent(AdsEvent::fullScreenAdDidHide);
			this->tryToPreloadInterstitialAd();
		}
		
	} else if (adType == RewardedVideo) {
		if (_rewardedIsShown) {
			CCLOG("Ad dismiss screen: %s", MTAdsManager::adNameForType(adType).c_str());
			_rewardedIsShown = false;
			
			_rewardedCallback = nullptr;
			
			dispatcher->dispatchCustomEvent(AdsEvent::fullScreenAdDidHide);
			this->tryToPreloadRewardedAd();
		}
	}
}

void MTAdsManager::onAdReward() {
	CCLOG("Ad reward should be received");
	GameDataManager::getInstance()->provideProduct(IAPProductKey::k_adVideoReward);
	if (_rewardedCallback) {
		_rewardedCallback(true);
		_rewardedCallback = nullptr;
	}
}

std::string MTAdsManager::adNameForType(AdType adType) {
	switch (adType) {
		case Banner:
			return "main_banner";
			break;
		case Interstitial:
			return "main_interstitial";
			break;
		case RewardedVideo:
			return "main_rewarded";
			break;
			
		default:
			CCASSERT(0, "Unexpected adType");
			return "";
			break;
	}
}

MTAdsManager::AdType MTAdsManager::adTypeForName(const std::string& name) {
	AdType adType = Banner;
	if (name == MTAdsManager::adNameForType(Banner)) {
		adType = Banner;
	} else if (name == MTAdsManager::adNameForType(Interstitial)) {
		adType = Interstitial;
	} else if (name == MTAdsManager::adNameForType(RewardedVideo)) {
		adType = RewardedVideo;
	}
	
	return adType;
}

#if (USE_APPODEAL == 1)

MTAppodeal::AdType MTAdsManager::appodealAdTypeForAdType(AdType adType) {
	MTAppodeal::AdType appodealAdType = MTAppodeal::AdType::Banner;
	switch (adType) {
		case Banner:
			appodealAdType = MTAppodeal::AdType::Banner;
			break;
		case Interstitial:
			appodealAdType = MTAppodeal::AdType::Interstitial;
			break;
		case RewardedVideo:
			appodealAdType = MTAppodeal::AdType::RewardedVideo;
			break;
			
		default:
			CCASSERT(0, "Unexpected AdType");
			break;
	}
	
	return appodealAdType;
}

#endif
