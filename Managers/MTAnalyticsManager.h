//
//  MTAnalyticsManager.hpp
//  WordTiles
//
//  Created by Pavel on 22/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTAnalyticsManager_hpp
#define MTAnalyticsManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../../MTDefines.h"

class MTAnalyticsManager {
public:
	
	static void initAndStartSession();
	
	static void logEvent(const std::string& eventName);
	static void logEvent(const std::string& eventName, std::map<std::string, std::string>& parameters);
	
private:
	
	
};

#endif /* MTAnalyticsManager_hpp */
