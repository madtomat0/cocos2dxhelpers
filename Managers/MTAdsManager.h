//
//  MTAdsManager.hpp
//  WordTiles
//
//  Created by Pavel on 10/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTAdsManager_hpp
#define MTAdsManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../../MTDefines.h"
#include "../Interface/IAppSettingsProvider.h"

#if (USE_APPODEAL == 1)
#include "../Helpers/MTAppodealHelper.h"
#else
#include "PluginAdMob/PluginAdMob.h"
#endif

struct AdsEvent {
	static const std::string bannerDidReceived;
    static const std::string fullScreenAdWillShow;
    static const std::string fullScreenAdDidHide;
};

class MTAdsManager:
#if (USE_APPODEAL == 1)
public MTAppodealListener
#else
public sdkbox::AdMobListener
#endif
{
public:
    ~MTAdsManager();
    MT_SINGLETON(MTAdsManager)
	void setup(IAppSettingsProvider* settingsProvider, int interstitialThreshold = 4);
    
    enum AdType { Banner = 0, Interstitial, RewardedVideo};
	
	typedef std::function<void (bool adWasShown)> AdShowCallback;
    
    void showAd(AdType adType);
    void hideAd();
	
	void disableAds(bool disable);
	bool adsAreDisabled() { return _settingsProvider->adsDisabled(); };
	
	float getBannerHeight();
	
    void tryToPreloadInterstitialAd();
	void tryToShowInterstitialAd(const AdShowCallback& callback);
    
    void tryToPreloadRewardedAd();
	void showRewardedAd(const AdShowCallback& callback);
	
	bool adIsAvailable(AdType adType);
    
private:
    const char* c_interstitialCountKey;
    
    bool init() { return true; };
    MTAdsManager();
    
    // Accessors
    void setInterstitialCount(int value);
	
	// MTAppodealListener
	virtual void onBannerDidLoadAd();
	virtual void onBannerDidFailToLoadAd();
	virtual void onBannerDidClick();
	virtual void onBannerPresent();
	
	virtual void onInterstitialDidLoadAd();
	virtual void onInterstitialDidFailToLoadAd();
	virtual void onInterstitialWillPresent();
	virtual void onInterstitialDidDismiss();
	virtual void onInterstitialDidClick();
	
    virtual void onRewardVideoDidLoadAd();
    virtual void onRewardVideoDidFailToLoadAd();
    virtual void onRewardVideoDidPresent();
    virtual void onRewardVideoWillDismiss();
    virtual void onRewardVideoDidFinish(int amount, const std::string& name);

	// AdMobListener
	virtual void adViewDidReceiveAd(const std::string &name);
	virtual void adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg);
	virtual void adViewWillPresentScreen(const std::string &name);
	virtual void adViewDidDismissScreen(const std::string &name);
	virtual void adViewWillDismissScreen(const std::string &name);
	virtual void adViewWillLeaveApplication(const std::string &name);
	virtual void reward(const std::string &name, const std::string &currency, double amount);

	// Helpers
	void onAdLoaded(AdType adType);
	void onAdFailedToLoad(AdType adType);
	void onAdWillPresentScreen(AdType adType);
	void onAdDismiss(AdType adType);
	void onAdReward();
	
	static std::string adNameForType(AdType adType);
	static AdType adTypeForName(const std::string& name);
	
#if (USE_APPODEAL == 1)
	MTAppodeal::AdType appodealAdTypeForAdType(AdType adType);
#endif
	
	bool _bannerWasCached;
	bool _bannerIsHidden;
	int _interstitialCount;
    bool _bannerAdIsPreloading;
    bool _interstitialAdIsPreloading;
    bool _rewardedAdIsPreloading;
	
	AdShowCallback _interstitialCallback;
	AdShowCallback _rewardedCallback;
    
    bool _interstitialIsShown;
    bool _rewardedIsShown;
	IAppSettingsProvider* _settingsProvider; // weak!
	int _interstitialThreshold;
};

#endif /* MTAdsManager_hpp */
