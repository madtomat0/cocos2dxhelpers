//
//  MTFontManager.cpp
//  LogicIntuition
//
//  Created by MAD Tomato on 17.03.15.
//
//

#include "MTFontManager.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTFontManager::MTFontManager()
: _currentRegularFontName("")
, _currentBoldFontName("")
, _currentRegularFontNameNumbers("")
, _currentBoldFontNameNumbers("")
{
	
}

MTFontManager::~MTFontManager() {
	
	CCLOG("~MTFontManager deleted");
}

// -------------------------------------
// MARK: Initialization

// singleton stuff
static MTFontManager* s_sharedManager = nullptr;

MTFontManager* MTFontManager::getInstance() {
	if (!s_sharedManager) {
		s_sharedManager = new (std::nothrow) MTFontManager();
		CCASSERT(s_sharedManager, "FATAL: Not enough memory");
		s_sharedManager->init();
	}
	
	return s_sharedManager;
}

bool MTFontManager::init() {
	CCLOG("MTFontManager init started");
	
	
	CCLOG("MTFontManager init finished");
	
	return true;
}

// -------------------------------------
// MARK: Accessors

void MTFontManager::setFontNames(const std::string& euroFontNameBase, const std::string& euroExtensionString, const std::string& asiaFontNameBase, const std::string& asiaExtensionString) {
	
	if (this->isAsianLanguage()) {
		_currentRegularFontName = StringUtils::format("%s-Regular%s", asiaFontNameBase.c_str(), asiaExtensionString.c_str());
		_currentBoldFontName = _currentRegularFontName;
		
		_currentRegularFontNameNumbers = StringUtils::format("%s-Regular%s", euroFontNameBase.c_str(), euroExtensionString.c_str());
		_currentBoldFontNameNumbers = _currentRegularFontNameNumbers;
		
	} else {
		_currentRegularFontName = StringUtils::format("%s-Regular%s", euroFontNameBase.c_str(), euroExtensionString.c_str());
		_currentBoldFontName = StringUtils::format("%s-Bold%s", euroFontNameBase.c_str(), euroExtensionString.c_str());
		
		_currentRegularFontNameNumbers = _currentRegularFontName;
		_currentBoldFontNameNumbers = _currentBoldFontName;
	}
}

std::string MTFontManager::getRegularFontName() {
	return _currentRegularFontName;
}

std::string MTFontManager::getBoldFontName() {
	return _currentBoldFontName;
}

std::string MTFontManager::getRegularFontNameNumbers() {
	return _currentRegularFontNameNumbers;
}

std::string MTFontManager::getBoldFontNameNumbers() {
	return _currentBoldFontNameNumbers;
}

// -------------------------------------
// MARK: Helper Methods

bool MTFontManager::isAsianLanguage() {
	LanguageType langType = Application::getInstance()->getCurrentLanguage();
	
	switch (langType) {
		case LanguageType::JAPANESE:
			return true;
			break;
		case LanguageType::KOREAN:
			return true;
			break;
		case LanguageType::CHINESE:
			return true;
			break;
			
		default:
			return false;
			break;
	}
}

bool MTFontManager::needToUseSpace() {
	LanguageType langType = Application::getInstance()->getCurrentLanguage();
	
	switch (langType) {
		case LanguageType::JAPANESE:
			return false;
			break;
		case LanguageType::CHINESE:
			return false;
			break;
			
		default:
			return true;
			break;
	}
}
