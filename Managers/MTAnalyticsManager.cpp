//
//  MTAnalyticsManager.cpp
//  WordTiles
//
//  Created by Pavel on 22/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTAnalyticsManager.h"

#if (USE_FLURRY == 1)
#include "PluginFlurryAnalytics/PluginFlurryAnalytics.h"
#endif

#if (USE_FIREBASE == 1)
#include "PluginFirebase/PluginFirebase.h"
#endif

// -------------------------------------
// MARK: General Methods

void MTAnalyticsManager::initAndStartSession() {
#if (USE_FLURRY == 1)
	sdkbox::PluginFlurryAnalytics::init();
	sdkbox::PluginFlurryAnalytics::startSession();
#endif
	
#if (USE_FIREBASE == 1)
	sdkbox::Firebase::Analytics::init();
#endif
}

void MTAnalyticsManager::logEvent(const std::string& eventName) {
#if (USE_FLURRY == 1)
	sdkbox::PluginFlurryAnalytics::logEvent(eventName);
#endif
	
#if (USE_FIREBASE == 1)
	std::map<std::string, std::string> parameters;
	sdkbox::Firebase::Analytics::logEvent(eventName, parameters);
#endif
	
}

void MTAnalyticsManager::logEvent(const std::string & eventName, std::map<std::string, std::string>& parameters) {
	
#if (USE_FLURRY == 1)
	sdkbox::PluginFlurryAnalytics::logEvent(eventName, parameters);
#endif
	
#if (USE_FIREBASE == 1)
	sdkbox::Firebase::Analytics::logEvent(eventName, parameters);
#endif
}
