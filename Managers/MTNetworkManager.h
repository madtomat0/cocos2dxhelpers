//
//  MTNetworkManager.hpp
//  WordTiles
//
//  Created by Paul Gorbunov on 29.05.17.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTNetworkManager_hpp
#define MTNetworkManager_hpp

#include "cocos2d.h"
#include "../../MTDefines.h"

class HttpClient;
class HttpResponse;

class MTNetworkManager {
public:
	~MTNetworkManager();
	MT_SINGLETON(MTNetworkManager)
	
	CC_SYNTHESIZE(int, _timeoutInterval, TimeoutInterval);
	
	typedef std::function<void (bool available)> AvailabilityCallback;
	
	void checkNetworkAvailability(const AvailabilityCallback& callback);
	
private:
	bool init();
	MTNetworkManager();
	
	
};

#endif /* MTNetworkManager_hpp */
