//
//  MTGameCloudManager.cpp
//  WordTiles
//
//  Created by Pavel on 24/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTGameCloudManager.h"
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "../Helpers/CCSerialization.h"
#include "../../Imported/Helpers/UriCodec.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTGameCloudManager::MTGameCloudManager()
: _signInCallback(nullptr)
, _loadUserDataCallback(nullptr)
, _saveUserDataCallback(nullptr)
, _loadDataTries(0)
, _loadedDataValueVector(ValueVectorNull)
, _packLoadInterval(0)

// Const
, c_signInTimeoutSchedule("MT_signInTimeoutSchedule")
, c_cloudLoadTimeoutSchedule("MT_cloudLoadTimeoutSchedule")
, c_cloudFullDataPackWaiter("MT_cloudFullDataPackWaiter")
, c_cloudLoadTimeout(5.0)
, c_maxPackLoadInterval(5.0)
, c_maxLoadTries(3)
{
	
}

MTGameCloudManager::~MTGameCloudManager() {
	_signInCallback = nullptr;
	_loadUserDataCallback = nullptr;
	_saveUserDataCallback = nullptr;
	
	CCLOG("~MTGameCloudManager deleted");
}

// -------------------------------------
// MARK: Initialization

MT_SINGLETON_IMPL(MTGameCloudManager)

bool MTGameCloudManager::init() {
	
	sdkbox::PluginSdkboxPlay::setListener(this);
	sdkbox::PluginSdkboxPlay::init();
	
	return true;
}

// -------------------------------------
// MARK: General Methods

void MTGameCloudManager::signIn(const SignInCallback& callback, float timeout) {
	CCLOG("Will sign in");
	if (this->isSignedIn()) {
		if (callback) {
			callback(true);
		}
	} else {
		_signInCallback = callback;
		sdkbox::PluginSdkboxPlay::signin();
		
		// Set timeout schedule
		Director::getInstance()->getScheduler()->schedule([this](float delta){
			this->onConnectionStatusChanged(sdkbox::GPS_CONNECTION_ERROR);
		}, this, timeout, false, c_signInTimeoutSchedule);
	}
}

bool MTGameCloudManager::isSignedIn() const {
	return sdkbox::PluginSdkboxPlay::isSignedIn();
}

// -------------------------------------
// MARK: Leaderboards

void MTGameCloudManager::submitScore(LeaderboardType leaderboardType, int score) {
	if (!this->isSignedIn()) {
		return;
	}
	
	sdkbox::PluginSdkboxPlay::submitScore(MTGameCloudManager::leaderboardNameForType(leaderboardType), score);
}

void MTGameCloudManager::showLeaderboard(LeaderboardType leaderboardType) {
	if (!this->isSignedIn()) {
		return;
	}
	
	sdkbox::PluginSdkboxPlay::showLeaderboard(MTGameCloudManager::leaderboardNameForType(leaderboardType));
}

// -------------------------------------
// MARK: Achievements

void MTGameCloudManager::unlockAchievement(const std::string& achievementName) const {
	if (!this->isSignedIn()) {
		return;
	}
	
	sdkbox::PluginSdkboxPlay::unlockAchievement(achievementName);
}

void MTGameCloudManager::showAchievements() {
	if (!this->isSignedIn()) {
		return;
	}
	
	sdkbox::PluginSdkboxPlay::showAchievements();
}

// -------------------------------------
// MARK:  User data cloud saving/loading

void MTGameCloudManager::saveUserDataToCloud(const std::string& saveName, const cocos2d::ValueMap& dataValueMap, const SaveDataCallback& callback) {
	
	_saveUserDataCallback = callback;
	
	if (!this->isSignedIn()) {
		this->onSaveGameData(false, "Not connected to play servises");
		return;
	}
	
	std::string dataString = StringUtils::getJSONStringFromValueMap(dataValueMap);
	std::string dataStringEncoded = StringUtils::UriEncode(dataString);
	
	const char* dataCString = dataStringEncoded.c_str();
	CCLOG("Will save to cloud:\n %s", dataStringEncoded.c_str());
	//sdkbox::PluginSdkboxPlay::saveGameData(saveName, dataString);
	const void* data = static_cast<const void*>(dataCString);
	int dataLength = (int)strlen(dataCString) + 1;
	
	sdkbox::PluginSdkboxPlay::saveGameDataBinary(saveName, data, dataLength);
}

void MTGameCloudManager::loadAllUserDataFromCloud(const LoadDataCallback& callback) {
	
	_loadedDataValueVector.clear();
	
	if (_loadDataTries == 0) {
		_loadDataTries = c_maxLoadTries;
	}
	
	_loadUserDataCallback = callback;
	
	if (!this->isSignedIn()) {
		this->onLoadGameData(nullptr, "Not connected to play servises");
		return;
	}
	
	CCLOG("Will try to load game data from cloud. Try N = %d", c_maxLoadTries - _loadDataTries);
	sdkbox::PluginSdkboxPlay::loadAllGameData();
	
	Director::getInstance()->getScheduler()->schedule([this](float delta){
		_loadDataTries--;
		if (_loadDataTries == 0) {
			this->onLoadGameData(nullptr, "Timeout error");
		} else {
			CCLOG("Will retry to load game data from cloud. Try N = %d", c_maxLoadTries - _loadDataTries);
			sdkbox::PluginSdkboxPlay::loadAllGameData();
		}
		
	}, this, c_cloudLoadTimeout, false, c_cloudLoadTimeoutSchedule);
}

void MTGameCloudManager::clearCloudCallbacks() {
	_loadUserDataCallback = nullptr;
	_saveUserDataCallback = nullptr;
}

void MTGameCloudManager::markUserDataAsDeleted(const std::string& saveName, const SaveDataCallback& callback) {
	
	this->saveUserDataToCloud(saveName, ValueMapNull, callback);
}

// -------------------------------------
// MARK: Helpers

std::string MTGameCloudManager::leaderboardNameForType(LeaderboardType leaderboardType) {
	switch (leaderboardType) {
		case Main:
			return "ldb1";
			break;
			
		default:
			return "";
			CCASSERT(0, "Unexpected leaderboard type");
			break;
	}
}

// -------------------------------------
// MARK: Listener

void MTGameCloudManager::onConnectionStatusChanged( int status ) {
	CCLOG("GPG Connection Status: %d", status);
//	*   + GPS_CONNECTED:       successfully connected.
//	*   + GPS_DISCONNECTED:    successfully disconnected.
//	*   + GPS_CONNECTION_ERROR:error with google play services connection.
	if (_signInCallback) {
		if (status == sdkbox::GPS_CONNECTED) {
			_signInCallback(true);
		} else {
			_signInCallback(false);
		}
	}
	
	_signInCallback = nullptr;
	
	if (Director::getInstance()->getScheduler()->isScheduled(c_signInTimeoutSchedule, this)) {
		Director::getInstance()->getScheduler()->unschedule(c_signInTimeoutSchedule, this);
	}
}

void MTGameCloudManager::onScoreSubmitted( const std::string& leaderboard_name, long score, bool maxScoreAllTime, bool maxScoreWeek, bool maxScoreToday ) {
	
}

void MTGameCloudManager::onIncrementalAchievementUnlocked( const std::string& achievement_name ) {
	
}

void MTGameCloudManager::onIncrementalAchievementStep( const std::string& achievement_name, double step ) {
	
}

void MTGameCloudManager::onAchievementUnlocked( const std::string& achievement_name, bool newlyUnlocked ) {
	
}

void MTGameCloudManager::onLoadGameData(const sdkbox::SavedGameData* savedData, const std::string& error) {
	
	_loadDataTries = 0;
	
	auto scheduler = Director::getInstance()->getScheduler();
	
	if (error == "" && savedData != nullptr) {
		char* charString = (char*)savedData->data;
		std::string dataString = std::string(charString);

		CCLOG("dataString: %s", dataString.c_str());
		
		std::string dataStringDecoded = StringUtils::UriDecode(dataString);
		CCLOG("dataStringDecoded: %s", dataStringDecoded.c_str());
		
		ValueMap dataValueMap = StringUtils::getValueMapFromJSONString(dataStringDecoded);
		if (dataValueMap != ValueMapNull) {
			_loadedDataValueVector.push_back(Value(dataValueMap));
		}
		
		_packLoadInterval = c_maxPackLoadInterval;
		CCLOG("Found cloud save");
		CCLOG("Reset pack load interval: %d", _packLoadInterval);
		
		// Schedule full data pack timer
		if (!scheduler->isScheduled(c_cloudFullDataPackWaiter, this)) {
			scheduler->schedule([this](float delta){
				
				_packLoadInterval--;
				CCLOG("... pack load interval: %d", _packLoadInterval);
				if (_packLoadInterval <= 0) {
					// Assume that all data was recieved
					Director::getInstance()->getScheduler()->unschedule(c_cloudFullDataPackWaiter, this);
					
					if (_loadUserDataCallback != nullptr) _loadUserDataCallback(true, _loadedDataValueVector);
					_loadUserDataCallback = nullptr;
				}
				
			}, this, 1.0, CC_REPEAT_FOREVER, 0, false, c_cloudFullDataPackWaiter);
		}
		
	} else {
		CCLOG("UserData load error: %s", error.c_str());
		if (_loadUserDataCallback != nullptr) _loadUserDataCallback(false, _loadedDataValueVector);
		_loadUserDataCallback = nullptr;
	}
	
	if (scheduler->isScheduled(c_cloudLoadTimeoutSchedule, this)) {
		scheduler->unschedule(c_cloudLoadTimeoutSchedule, this);
	}
}

void MTGameCloudManager::onSaveGameData(bool success, const std::string& error) {
	CCLOG("Cloud save game data callback");
	if (!success) {
		CCLOG("UserData save error: %s", error.c_str());
	}
	
	if (_saveUserDataCallback != nullptr) _saveUserDataCallback(success);
	_saveUserDataCallback = nullptr;
}
