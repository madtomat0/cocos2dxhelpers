//
//  MTIAPManager.hpp
//  WordTiles
//
//  Created by Pavel on 10/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTIAPManager_hpp
#define MTIAPManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../../MTDefines.h"
#include "PluginIAP/PluginIAP.h"
#include "../Data/MTIAPProduct.h"

typedef std::function<void (bool success, MTError* error, MTIAPProduct* product)> MTIAPPurchaseCallback;
typedef std::function<void (bool success, MTError* error)> MTIAPRestoreCallback;
typedef std::function<void (bool success, MTError* error, const cocos2d::Vector<MTIAPProduct*> products)> MTIAPRefreshCallback;

class MTIAPManager: public sdkbox::IAPListener {
public:
    ~MTIAPManager();
    MT_SINGLETON(MTIAPManager)
    
    void refreshIAPData(MTIAPRefreshCallback callback);
    void restorePurchases(MTIAPRestoreCallback callback);
    void purchaseProduct(const std::string& productName, MTIAPPurchaseCallback callback);
	
	bool productIsPurchased(const MTIAPProduct* product);
	
	void clearAllCallbacks();
	
	MTIAPProduct* productForProductKey(const std::string& productKey);
    
private:
    bool init();
    MTIAPManager();
    
    // IAPListener
    virtual void onInitialized(bool ok) override;
    virtual void onSuccess(sdkbox::Product const& p) override;
    virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
    virtual void onCanceled(sdkbox::Product const& p) override;
    virtual void onRestored(sdkbox::Product const& p) override;
    virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
    virtual void onProductRequestFailure(const std::string &msg) override;
    virtual void onRestoreComplete(bool ok, const std::string &msg) override;
	
	// Helpers
	MTIAPProduct* processPurchasedProduct(sdkbox::Product const& p);
	void logProducts(const cocos2d::Vector<MTIAPProduct*>& products);
	
	MTIAPPurchaseCallback _purchaseCallback;
	MTIAPRestoreCallback _restoreCallback;
	MTIAPRefreshCallback _refreshCallback;
	
	cocos2d::Vector<MTIAPProduct*> _purchasedProducts;
	cocos2d::Vector<MTIAPProduct*> _refreshedProducts;
};

#endif /* MTIAPManager_hpp */
