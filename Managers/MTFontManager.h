//
//  MTFontManager.h
//  LogicIntuition
//
//  Created by MAD Tomato on 17.03.15.
//
//

#ifndef __LogicIntuition__MTFontManager__
#define __LogicIntuition__MTFontManager__

#include "cocos2d.h"
#include "../../MTDefines.h"

class MTFontManager : public cocos2d::Ref {
public:
	/** returns a shared instance */
	static MTFontManager* getInstance();
	~MTFontManager();
	
	void setFontNames(const std::string& euroFontNameBase, const std::string& euroExtensionString, const std::string& asiaFontNameBase, const std::string& asiaExtensionString);
	
	std::string getRegularFontName();
	std::string getBoldFontName();
	std::string getRegularFontNameNumbers();
	std::string getBoldFontNameNumbers();
	
	bool isAsianLanguage();
	bool needToUseSpace();
	
private:
	bool init();
	MTFontManager();
	
	std::string _currentRegularFontName;
	std::string _currentBoldFontName;
	
	std::string _currentRegularFontNameNumbers;
	std::string _currentBoldFontNameNumbers;
};

#endif /* defined(__LogicIntuition__MTFontManager__) */
