//
//  MTGameCloudManager.hpp
//  WordTiles
//
//  Created by Pavel on 24/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef MTGameCloudManager_hpp
#define MTGameCloudManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "../../MTDefines.h"
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"

class MTGameCloudManager: public sdkbox::SdkboxPlayListener {
public:
	~MTGameCloudManager();
	MT_SINGLETON(MTGameCloudManager)
	
	typedef std::function<void (bool success)> SignInCallback;
	typedef std::function<void (bool success, const cocos2d::ValueVector& loadedDataValueVector)> LoadDataCallback;
	typedef std::function<void (bool success)> SaveDataCallback;
	
	enum LeaderboardType { Main = 0 };
	
	void signIn(const SignInCallback& callback, float timeout = 10);
	bool isSignedIn() const;
	
	// Leaderboards
	void submitScore(LeaderboardType leaderboardType, int score);
	void showLeaderboard(LeaderboardType leaderboardType);
	
	// Achievements
    void unlockAchievement(const std::string& achievementName) const;
	void showAchievements();
	
	// User data cloud saving/loading
	void saveUserDataToCloud(const std::string& saveName, const cocos2d::ValueMap& dataValueMap, const SaveDataCallback& callback);
	void loadAllUserDataFromCloud(const LoadDataCallback& callback);
    void clearCloudCallbacks();
	void markUserDataAsDeleted(const std::string& saveName, const SaveDataCallback& callback);
	
private:
    const std::string c_signInTimeoutSchedule;
    const std::string c_cloudLoadTimeoutSchedule;
	const std::string c_cloudFullDataPackWaiter;
    const float c_cloudLoadTimeout;
	const float c_maxPackLoadInterval;
	const int c_maxLoadTries;
    
	bool init();
	MTGameCloudManager();
	
	// Listener
	virtual void onConnectionStatusChanged( int status ) override;
	virtual void onScoreSubmitted( const std::string& leaderboard_name, long score, bool maxScoreAllTime, bool maxScoreWeek, bool maxScoreToday ) override;
	virtual void onIncrementalAchievementUnlocked( const std::string& achievement_name ) override;
	virtual void onIncrementalAchievementStep( const std::string& achievement_name, double step ) override;
	virtual void onAchievementUnlocked( const std::string& achievement_name, bool newlyUnlocked ) override;
    
    virtual void onLoadGameData(const sdkbox::SavedGameData* savedData, const std::string& error) override;
    virtual void onSaveGameData(bool success, const std::string& error) override;
	
	static std::string leaderboardNameForType(LeaderboardType leaderboardType);
	
	SignInCallback _signInCallback;
	LoadDataCallback _loadUserDataCallback;
	SaveDataCallback _saveUserDataCallback;
	
	int _loadDataTries;
	cocos2d::ValueVector _loadedDataValueVector;
	int _packLoadInterval;
};

#endif /* MTGameCloudManager_hpp */
