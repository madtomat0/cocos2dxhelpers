//
//  MTIAPManager.cpp
//  WordTiles
//
//  Created by Pavel on 10/03/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTIAPManager.h"
#include "../../Model/Managers/GameDataManager.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTIAPManager::MTIAPManager()
: _purchaseCallback(nullptr)
, _restoreCallback(nullptr)
, _refreshCallback(nullptr)
, _purchasedProducts(Vector<MTIAPProduct*>())
, _refreshedProducts(Vector<MTIAPProduct*>())
{
	
}

MTIAPManager::~MTIAPManager() {
	sdkbox::IAP::removeListener();
	
	_purchaseCallback = nullptr;
	_restoreCallback = nullptr;
	_refreshCallback = nullptr;
	
	_purchasedProducts.clear();
	_refreshedProducts.clear();
	
	CCLOG("~MTIAPManager deleted");
}

// -------------------------------------
// MARK: Initialization

MT_SINGLETON_IMPL(MTIAPManager)

bool MTIAPManager::init() {
	sdkbox::IAP::setListener(this);
	sdkbox::IAP::init();
	
	return true;
}

// -------------------------------------
// MARK: General Methods

void MTIAPManager::refreshIAPData(MTIAPRefreshCallback callback) {
	if (_refreshedProducts.size() > 0) {
		CCLOG("No need to refresh IAP data. Will use cached one.");
		callback(true, nullptr, _refreshedProducts);
	} else {
		CCLOG("Will refresh IAP data");
		_refreshCallback = callback;
		sdkbox::IAP::refresh();
	}
}

void MTIAPManager::restorePurchases(MTIAPRestoreCallback callback) {
	
	CCLOG("Will restore purchases");
	_purchasedProducts.clear();
	
	_restoreCallback = callback;
	sdkbox::IAP::restore();
}

void MTIAPManager::purchaseProduct(const std::string& productName, MTIAPPurchaseCallback callback) {
	CCLOG("Will purchase product: %s", productName.c_str());
	_purchaseCallback = callback;
	sdkbox::IAP::purchase(productName);
}

// -------------------------------------
// MARK: IAPListener

// IAP Initialization
void MTIAPManager::onInitialized(bool ok) {
	CCLOG("IAP initialization %s", (ok) ? "success" : "failure");
	if (ok) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		this->restorePurchases(nullptr);
#endif
	}
}

// Products purchase
void MTIAPManager::onSuccess(sdkbox::Product const& p) {
	CCLOG("Product purchase success: %s", p.name.c_str());
	auto product = this->processPurchasedProduct(p);
	
	if (_purchaseCallback) {
		_purchaseCallback(true, nullptr, product);
	}
	
	_purchaseCallback = nullptr;
}

void MTIAPManager::onFailure(sdkbox::Product const& p, const std::string &msg) {
	CCLOG("Product %s purchase failure: %s", p.name.c_str(), msg.c_str());
	if (_purchaseCallback) {
		MTError* error = MTError::create(msg);
		_purchaseCallback(false, error, nullptr);
	}
	
	_purchaseCallback = nullptr;
}

void MTIAPManager::onCanceled(sdkbox::Product const& p) {
	CCLOG("Product purchase cancelled: %s", p.name.c_str());
	if (_purchaseCallback) {
		_purchaseCallback(false, nullptr, nullptr);
	}
	
	_purchaseCallback = nullptr;
}

void MTIAPManager::onRestored(sdkbox::Product const& p) {
	CCLOG("Product restored: %s", p.name.c_str());
	auto product = this->processPurchasedProduct(p);
	
	if (_purchaseCallback) {
		_purchaseCallback(true, nullptr, product);
	}
	
	_purchaseCallback = nullptr;
}

// Products refresh
void MTIAPManager::onProductRequestSuccess(std::vector<sdkbox::Product> const &products) {
	CCLOG("Products request success. Products list:");
	_refreshedProducts = MTIAPProduct::mapProducts(products);
	this->logProducts(_refreshedProducts);
	
	if (_refreshCallback) {
		_refreshCallback(true, nullptr, _refreshedProducts);
	}
	
	_refreshCallback = nullptr;
}

void MTIAPManager::onProductRequestFailure(const std::string &msg) {
	CCLOG("Products request failure: %s", msg.c_str());
	
	if (_refreshCallback) {
		MTError* error = MTError::create(msg);
		_refreshCallback(false, error, Vector<MTIAPProduct*>());
	}
	
	_refreshCallback = nullptr;
}

// Products restore
void MTIAPManager::onRestoreComplete(bool ok, const std::string &msg) {
	if (ok) {
		CCLOG("Products restore complete: %s", msg.c_str());
		this->logProducts(_purchasedProducts);
	} else {
		CCLOG("Products restore failure: %s", msg.c_str());
	}
	
	if (_restoreCallback) {
		MTError* error = (ok) ? nullptr : MTError::create(msg);
		_restoreCallback(ok, error);
	}
	
	_restoreCallback = nullptr;
}

// -------------------------------------
// MARK: General Methods

bool MTIAPManager::productIsPurchased(const MTIAPProduct* product) {
	for (auto purchasedProduct : _purchasedProducts) {
		if (purchasedProduct->getName() == product->getName()) {
			return true;
		}
	}
	
	return false;
}

void MTIAPManager::clearAllCallbacks() {
	_purchaseCallback = nullptr;
	_restoreCallback = nullptr;
	_refreshCallback = nullptr;
}

MTIAPProduct* MTIAPManager::productForProductKey(const std::string& productKey) {
	
	for (auto product : _refreshedProducts) {
		if (product->getName() == productKey) {
			return product;
		}
	}
	
	return nullptr;
}

// -------------------------------------
// MARK: Helpers

MTIAPProduct* MTIAPManager::processPurchasedProduct(sdkbox::Product const& p) {
	auto product = MTIAPProduct::create()->setup(p);
	
	GameDataManager::getInstance()->provideProduct(product->getName());
	if (product->getType() == MTIAPProduct::Type::Nonconsumable) {
		_purchasedProducts.pushBack(product);
	}
	
	return product;
}

void MTIAPManager::logProducts(const cocos2d::Vector<MTIAPProduct*>& products) {
	for (auto product : products) {
		CCLOG("- %s (%s -- %s)", product->getName().c_str(), product->getTitle().c_str(), product->getDescription().c_str());
	}
}
