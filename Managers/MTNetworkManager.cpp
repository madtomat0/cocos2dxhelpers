//
//  MTNetworkManager.cpp
//  WordTiles
//
//  Created by Paul Gorbunov on 29.05.17.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#include "MTNetworkManager.h"
#include <cocos/network/HttpRequest.h>
#include <cocos/network/HttpClient.h>

USING_NS_CC;
using namespace cocos2d::network;

#define DEFAULT_TIMEOUT 5

// -------------------------------------
// MARK: Ctors

MTNetworkManager::MTNetworkManager()
: _timeoutInterval(DEFAULT_TIMEOUT)
{
	
}

MTNetworkManager::~MTNetworkManager() {
	
	
	CCLOG("~MTNetworkManager deleted");
}

// -------------------------------------
// MARK: Initialization

MT_SINGLETON_IMPL(MTNetworkManager)

bool MTNetworkManager::init() {
	
	return true;
}

// -------------------------------------
// MARK: General Methods

void MTNetworkManager::checkNetworkAvailability(const AvailabilityCallback& callback) {
	HttpRequest* request = new (std::nothrow) HttpRequest();
	request->setUrl("http://www.google.com");
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback([callback](cocos2d::network::HttpClient* client, cocos2d::network::HttpResponse* response) {
		bool notAvailable = (!response || !response->isSucceed());
		CCLOG("Network connection is %s", (notAvailable) ? "NOT available." : "available.");
		callback(!notAvailable);
	});
	cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(_timeoutInterval);
	cocos2d::network::HttpClient::getInstance()->send(request);
	request->release();
}
