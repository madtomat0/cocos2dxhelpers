//
//  MTButton.cpp
//  SmartMagesBattle
//
//  Created by Paul Gorbunov on 28.11.16.
//
//

#include "MTButton.h"

USING_NS_CC;
using namespace cocos2d::ui;

// -------------------------------------
// MARK: Ctors

MTButton::MTButton()
: _disabledOpacity(140)
{
	
}

MTButton::~MTButton() {
	
	if (this->isScheduled("kMTButtonDisableSchedule")) {
		this->unschedule("kMTButtonDisableSchedule");
	}
}

// -------------------------------------
// MARK: Initialization

MTButton* MTButton::create(const std::string &normalImage,
					   const std::string& selectedImage ,
					   const std::string& disableImage,
					   TextureResType texType) {
	MTButton *btn = new (std::nothrow) MTButton;
	if (btn && btn->init(normalImage,selectedImage,disableImage,texType))
	{
		btn->autorelease();
		return btn;
	}
	CC_SAFE_DELETE(btn);
	return nullptr;
}

bool MTButton::init(const std::string &normalImage,
				  const std::string& selectedImage ,
				  const std::string& disableImage,
				  TextureResType texType) {
	if (!Button::init(normalImage, selectedImage, disableImage, texType)) {
		return false;
	}
	
	_enableSquareTouchArea = false;
	
	return true;
}

// -------------------------------------
// MARK: Overriden Methods

void MTButton::setEnabled(bool enabled) {
	Button::setEnabled(enabled);
	
	GLubyte opacity = (enabled) ? 255 : _disabledOpacity;
	this->setOpacity(opacity);
}

bool MTButton::hitTest(const Vec2 &pt, const Camera* camera, Vec3 *p) const {
	Rect rect;
	
	if (_enableSquareTouchArea) {
		float maxSide = MAX(getBoundingBox().size.height, getBoundingBox().size.width);
		rect.size = Size(maxSide, maxSide);
	} else {
		rect.size = getContentSize();
	}
	
	return isScreenPointInRect(pt, camera, getWorldToNodeTransform(), rect, p);
}

// -------------------------------------
// MARK: General Methods

void MTButton::disableForTimePeriod(float disableTime) {
	if (disableTime > 0) {
		this->setEnabled(false);
		this->scheduleOnce([this](float delta){
			this->setEnabled(true);
		}, disableTime, "kMTButtonDisableSchedule");
	}
}

void MTButton::setBackgroundColor(const Color3B& color) {
	_buttonNormalRenderer->updateDisplayedColor(color);
	_buttonClickedRenderer->updateDisplayedColor(color);
	_buttonDisabledRenderer->updateDisplayedColor(color);
}
