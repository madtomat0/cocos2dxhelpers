//
//  MTTextView.h
//  Xonix
//
//  Created by MAD Tomato on 15.01.15.
//
//

#ifndef __Xonix__MTTextView__
#define __Xonix__MTTextView__

#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>

enum MTTextAlign {
	MTTextAlignLeft = 0,
	MTTextAlignCenter,
	MTTextAlignRight
};

struct MTTextViewParams {
	std::string text;
	float maxWidth;
	std::string fontName;
	float fontSize;
	cocos2d::Color3B textColor;
	cocos2d::Color4B bgColor;
	float lineInterval;
	MTTextAlign align;
	
	MTTextViewParams();
	MTTextViewParams(const std::string& text, float maxWidth, const std::string& fontName, float fontSize, cocos2d::Color3B textColor = defaultTextColor, cocos2d::Color4B bgColor = defaultBgColor, float lineInterval = defaultLineInterval, MTTextAlign align = MTTextAlignLeft);
	~MTTextViewParams();
	
	static const float defaultLineInterval;
	static const cocos2d::Color3B defaultTextColor;
	static const cocos2d::Color4B defaultBgColor;
};

class MTTextView : public cocos2d::ui::Layout {
public:
	static MTTextView* create(const MTTextViewParams& parameters);
	~MTTextView();
	
	float getActualWidth();
	
	// Overriden
	virtual void setOpacity(GLubyte opacity);
	
private:
	bool init(const MTTextViewParams& parameters);
	MTTextView();
	
	void setupView();
	
	std::vector<std::string> divideString(const std::string& inputString);
	
	MTTextViewParams _parameters;
	GLubyte _initialBgOpacity;
};

#endif /* defined(__Xonix__MTTextView__) */
