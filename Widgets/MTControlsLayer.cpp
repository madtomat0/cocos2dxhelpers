//
//  MTControlsLayer.cpp
//  CoolSquare
//
//  Created by MAD Tomato on 03.01.15.
//
//

#include "MTControlsLayer.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

MTControlsLayer::MTControlsLayer()
: _listener(nullptr)
, _swipeDirection(MTSwipeDirectionNONE)
, _swipeInAction(false)
, _delegate(nullptr)
, _touchStartLocation(Vec2::ZERO)
, _longTouchIsPossible(false)
, _longTouchHappend(false)
{
	
}

MTControlsLayer::~MTControlsLayer() {
	auto dispatcher = this->getEventDispatcher();
	dispatcher->removeEventListener(_listener);
	_listener = nullptr;
	_delegate = nullptr;
	
	CCLOG("~MTControlsLayer deleted");
}

// -------------------------------------
// MARK: Initialization

MTControlsLayer* MTControlsLayer::createWithDelegate(MTControlsLayerDelegate *delegate) {
	MTControlsLayer* layer = new MTControlsLayer();
	if (layer && layer->initWithDelegate(delegate)) {
		layer->autorelease();
		return layer;
	}
	CC_SAFE_DELETE(layer);
	return NULL;
}

bool MTControlsLayer::initWithDelegate(MTControlsLayerDelegate* delegate) {
	if (!Layer::init()) {
		return false;
	}
	
	_delegate = delegate;
	
	_listener = EventListenerTouchOneByOne::create();
	_listener->onTouchBegan = CC_CALLBACK_2(MTControlsLayer::onTouchBegan, this);
	_listener->onTouchMoved = CC_CALLBACK_2(MTControlsLayer::onTouchMoved, this);
	_listener->onTouchEnded = CC_CALLBACK_2(MTControlsLayer::onTouchEnded, this);
	_listener->onTouchCancelled = CC_CALLBACK_2(MTControlsLayer::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
	
	this->setPropagateTouches(false);
	
	return true;
}

// -------------------------------------
// MARK: Accessors

void MTControlsLayer::setPropagateTouches(bool propagateTouches) {
	_listener->setSwallowTouches(!propagateTouches);
}

// -------------------------------------
// MARK: Helpers

void MTControlsLayer::cancelLongTouch() {
	if (_longTouchIsPossible) {
		_longTouchIsPossible = false;
		this->unschedule(c_longTouchSchedule);
	}
}

// -------------------------------------
// MARK: Touches Handling

bool MTControlsLayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event) {
	_touchStartLocation = touch->getLocation();
	_touchLocation = _touchStartLocation;
	_longTouchIsPossible = true;
	
	_delegate->touchWasStarted(this);
	
	// Delayed Touch
	this->schedule([this](float dt){
		if (_swipeInAction || _longTouchHappend) {
			this->unschedule(c_touchSchedule);
			_longTouchHappend = false;
			_longTouchIsPossible = false;
		} else if (!_swipeInAction && !_longTouchIsPossible) {
			_delegate->touchWasDetected(this);
			this->unschedule(c_touchSchedule);
			
		}
	}, TOUCH_DELAY, c_touchSchedule);
	
	// Long Touch
	this->scheduleOnce([this](float dt){
		if (_longTouchIsPossible) {
			
			_delegate->longTouchWasDetected(this);
			_longTouchHappend = true;
		}
	}, c_longTouchTime, c_longTouchSchedule);
	
	return true;
}

void MTControlsLayer::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event) {
	float dtX = touch->getLocation().x - touch->getStartLocation().x;
	float dtY = touch->getLocation().y - touch->getStartLocation().y;
	
	_touchLocation = touch->getLocation();
	
	if (!_swipeInAction) {
		if (MAX(fabsf(dtX), fabsf(dtY)) > SWIPE_THRESHOLD) {
			// Swipe happend
			_swipeInAction = true;
			this->cancelLongTouch();
			if (fabsf(dtX) > fabsf(dtY)) {
				_swipeDirection = (dtX > 0) ? MTSwipeDirectionRight : MTSwipeDirectionLeft;
			} else {
				_swipeDirection = (dtY > 0) ? MTSwipeDirectionUp : MTSwipeDirectionDown;
			}
			
			if (_delegate != nullptr) {
				_delegate->swipeWasDetected(this);
			}
		}
	}
	
	_delegate->touchDidMove(this);
}

void MTControlsLayer::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event) {
	_touchLocation = touch->getLocation();
	
	_swipeInAction = false;
	this->cancelLongTouch();
	_delegate->touchWasFinished(this);
}

void MTControlsLayer::onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event) {
	_touchLocation = touch->getLocation();
	
	_swipeInAction = MTSwipeDirectionNONE;
	_swipeInAction = false;
	_touchStartLocation = Vec2::ZERO;
	this->cancelLongTouch();
	_delegate->touchWasFinished(this);
}

// -------------------------------------
// MARK: Accessors

MTSwipeDirection MTControlsLayer::getSwipeDirection() {
	return _swipeDirection;
}

Vec2 MTControlsLayer::getTouchStartLocation() {
	return _touchStartLocation;
}

Vec2 MTControlsLayer::getTouchLocation() {
	return _touchLocation;
}

