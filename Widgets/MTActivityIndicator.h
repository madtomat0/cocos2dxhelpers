//
//  MTActivityIndicator.h
//  Xonix
//
//  Created by MAD Tomato on 20.01.15.
//
//

#ifndef __Xonix__MTActivityIndicator__
#define __Xonix__MTActivityIndicator__

#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>
#include "../../MTDefines.h"

class MTActivityIndicator : public cocos2d::ui::Layout {
public:
	static MTActivityIndicator* create(const std::string& spinnerImageFrameName, const std::string& labelString);
	~MTActivityIndicator();
	
	void showOnNode(cocos2d::Node* parentNode, int zOrder = 0);
	void hide();
	
private:
	const GLubyte c_bgOpacity = 150;
	const cocos2d::Size c_indSize = cocos2d::Size(200.0, 120.0);
	const cocos2d::Size c_spinnerSize = cocos2d::Size(25.0, 25.0);
	const float c_showDuration = 0.4;
	
	bool init(const std::string& spinnerImageFrameName, const std::string& labelString);
	MTActivityIndicator();
	
	cocos2d::Sprite* _bgSprite;
	cocos2d::Sprite* _spinner;
};

#endif /* defined(__Xonix__MTActivityIndicator__) */
