//
//  MTCollectionView.hpp
//
//
//  Created by Paul Gorbunov on 20.02.17.
//
//

#ifndef MTCollectionView_hpp
#define MTCollectionView_hpp

#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>
#include "../../MTDefines.h"
#include "../Base/MTBaseCell.h"

class MTCollectionViewDataSource;
class MTCollectionViewDelegate;

class MTCollectionView: public cocos2d::ui::ScrollView {
public:
	~MTCollectionView();
	CREATE_FUNC(MTCollectionView);
	
	CC_SYNTHESIZE(MTCollectionViewDataSource*, _dataSource, DataSource);
	CC_SYNTHESIZE(MTCollectionViewDelegate*, _delegate, Delegate);
	CC_SYNTHESIZE(cocos2d::ui::Margin, _cellsMargin, CellsMargin);
	
	MTCollectionView* setup(const cocos2d::Size& size, ScrollView::Direction direction);
	
	void reloadData();
	void showItemAtIndex(int index, float scrollDuration = 0.4);
	
	MTBaseCell* dequeueReusableCell(const std::string& cellReuseId);
	
private:
	MTCollectionView();
	
	cocos2d::ValueMap _reusableCells;
	int _itemsInRow;
};

class MTCollectionViewDataSource {
public:
	virtual int numberOfItems(MTCollectionView* collectionView) = 0;
	virtual MTBaseCell* cellForItemAtIndex(int itemIndex, MTCollectionView* collectionView) = 0;
    virtual cocos2d::Size sizeForItemAtIndex(int itemIndex, MTCollectionView* collectionView) { return cocos2d::Size::ZERO;};
};

class MTCollectionViewDelegate {
public:
	// optional
	virtual void collectionViewDidSelectItemAt(int itemIndex, MTCollectionView* collectionView) {};
};

#endif /* MTCollectionView_hpp */
