//
//  MTControlsLayer.h
//  CoolSquare
//
//  Created by MAD Tomato on 03.01.15.
//
//

#ifndef __CoolSquare__MTControlsLayer__
#define __CoolSquare__MTControlsLayer__

#include "cocos2d.h"
#include "../../MTDefines.h"

class MTControlsLayerDelegate;

class MTControlsLayer : public cocos2d::Layer {
public:
	static MTControlsLayer* createWithDelegate(MTControlsLayerDelegate* delegate);
	~MTControlsLayer();
	
	// Accessors
	void setPropagateTouches(bool propagateTouches);
	
	MTSwipeDirection getSwipeDirection();
	cocos2d::Vec2 getTouchStartLocation();
	cocos2d::Vec2 getTouchLocation();
	
private:
	const float SWIPE_THRESHOLD = 5.0;
	const float TOUCH_DELAY = 0.04;
	const float c_longTouchTime = 0.8;
	
	const std::string c_touchSchedule = "MTTouchSchedule";
	const std::string c_longTouchSchedule = "MTLongTouchSchedule";
	
	MTControlsLayer();
	bool initWithDelegate(MTControlsLayerDelegate* delegate);
	
	void cancelLongTouch();
	
	virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
	virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
	virtual void onTouchCancelled(cocos2d::Touch* touch, cocos2d::Event* event);
	
	cocos2d::EventListenerTouchOneByOne* _listener;
	MTSwipeDirection _swipeDirection;
	bool _swipeInAction;
	MTControlsLayerDelegate* _delegate;
	cocos2d::Vec2 _touchStartLocation;
	cocos2d::Vec2 _touchLocation;
	bool _longTouchIsPossible;
	bool _longTouchHappend;
};

class MTControlsLayerDelegate {
public: // optional methods
	virtual void swipeWasDetected(MTControlsLayer* sender) {};
	virtual void touchWasDetected(MTControlsLayer* sender) {};
	virtual void longTouchWasDetected(MTControlsLayer* sender) {};
	virtual void touchWasStarted(MTControlsLayer* sender) {};
	virtual void touchDidMove(MTControlsLayer* sender) {};
	virtual void touchWasFinished(MTControlsLayer* sender) {};
};

#endif /* defined(__CoolSquare__MTControlsLayer__) */

