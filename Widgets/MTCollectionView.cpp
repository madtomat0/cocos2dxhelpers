//
//  MTCollectionView.cpp
//
//
//  Created by Paul Gorbunov on 20.02.17.
//
//

#include "MTCollectionView.h"

USING_NS_CC;
using namespace cocos2d::ui;

#define DEFAULT_MARGIN 5.0

// -------------------------------------
// MARK: Ctors

MTCollectionView::MTCollectionView()
: _dataSource(nullptr)
, _delegate(nullptr)
, _cellsMargin(Margin(DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN))
, _itemsInRow(0)
{
	
}

MTCollectionView::~MTCollectionView() {
	_dataSource = nullptr;
	_delegate = nullptr;
	
	CCLOG("~MTCollectionView deleted");
}

// -------------------------------------
// MARK: Setup

MTCollectionView* MTCollectionView::setup(const cocos2d::Size& size, ScrollView::Direction direction) {
	this->setContentSize(size);
	
	this->setBounceEnabled(true);
	
	this->setDirection(direction);
	if (direction == Direction::HORIZONTAL) {
		this->setLayoutType(Layout::Type::HORIZONTAL);
	} else if (direction == Direction::VERTICAL) {
		this->setLayoutType(Layout::Type::VERTICAL);
	} else {
		CCASSERT(0, "Unsupported direction type");
	}
	
	// Test colors
	this->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	this->setBackGroundColor(Color3B::GREEN);
	
	return this;
}

// -------------------------------------
// MARK: General Methods

void MTCollectionView::reloadData() {
	this->removeAllChildren();
	
	int itemsQty = _dataSource->numberOfItems(this);
	
	if (itemsQty == 0) {
		return;
	}
	
	float scrollViewWidth = mt_boundsWidth(this);
	
	auto tempCell = _dataSource->cellForItemAtIndex(0, this);
	float cellWidth = mt_boundsWidth(tempCell);
	float cellHeight = mt_boundsHeight(tempCell);
	
	_itemsInRow = int(scrollViewWidth / (_cellsMargin.left + cellWidth + _cellsMargin.right));
	CCASSERT(_itemsInRow > 0, "Can't fit item in collection view's row. Width is too big.");
	int rowsQty = int(ceilf(float(itemsQty) / float(_itemsInRow)));
	float rowHeight = _cellsMargin.top + cellHeight + _cellsMargin.bottom;
	
	if (this->getLayoutType() == Layout::Type::VERTICAL) {
		this->setInnerContainerSize(Size(scrollViewWidth, rowHeight * rowsQty));
	} else if (this->getLayoutType() == Layout::Type::HORIZONTAL) {
		this->setInnerContainerSize(Size(scrollViewWidth * rowsQty, mt_boundsHeight(this)));
	} else {
		CCASSERT(0, "Unsupported layout type");
	}
	
	for (int row = 0; row < rowsQty; row++) {
		auto rowLayout = Layout::create();
		rowLayout->setContentSize(Size(scrollViewWidth, rowHeight));
		rowLayout->setLayoutType(Layout::Type::HORIZONTAL);
		rowLayout->setBackGroundColorType(Layout::BackGroundColorType::NONE);
		//rowLayout->setBackGroundColor((row % 2 == 0) ? Color3B::BLUE : Color3B::GRAY);
		
		auto lp = LinearLayoutParameter::create();
		lp->setGravity(LinearLayoutParameter::LinearGravity::CENTER_HORIZONTAL);
		lp->setMargin(Margin(0, 0, 0, 0));
		rowLayout->setLayoutParameter(lp);
		
		this->addChild(rowLayout);
		
		for (int col = 0; col < _itemsInRow; col++) {
			
			int itemIndex = row * _itemsInRow + col;
			if (itemIndex < itemsQty) {
				
				auto cell = _dataSource->cellForItemAtIndex(itemIndex, this);
				
				auto lp = LinearLayoutParameter::create();
				lp->setGravity(LinearLayoutParameter::LinearGravity::CENTER_VERTICAL);
				lp->setMargin(Margin(_cellsMargin));
				cell->setLayoutParameter(lp);
				
				rowLayout->addChild(cell);
			}
		}
	}
}

void MTCollectionView::showItemAtIndex(int index, float scrollDuration) {
	int itemsQty = _dataSource->numberOfItems(this);
	int rowsQty = int(ceilf(float(itemsQty) / float(_itemsInRow)));
	
	int itemRowNumber = int(floorf(float(index) / float(_itemsInRow)));
	
	float percent = (float(itemRowNumber) / float(rowsQty)) * 100.0;
	if (percent < 10) {
		percent = 0.0;
	} else if (percent > 90) {
		percent = 100.0;
	}
	
	if (percent > 0) {
		if (this->getDirection() == ScrollView::Direction::VERTICAL) {
			this->scrollToPercentVertical(percent, scrollDuration, true);
		} else if (this->getDirection() == ScrollView::Direction::HORIZONTAL) {
			this->scrollToPercentHorizontal(percent, scrollDuration, true);
		}
	}
}

MTBaseCell* MTCollectionView::dequeueReusableCell(const std::string& cellReuseId) {
	//TODO: Implement cell pool
	return nullptr;
}
