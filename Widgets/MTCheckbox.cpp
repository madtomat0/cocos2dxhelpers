//
//  MTCheckbox.cpp
//  LogicIntuition
//
//  Created by MAD Tomato on 19.02.15.
//
//

#include "MTCheckbox.h"
#include "../../MTDefines.h"

USING_NS_CC;

MTCheckbox::MTCheckbox()
: _bgSprite(nullptr)
, _selectedFrameName("")
, _deselectedFrameName("")
, _listener(nullptr)
, _currentState(MTCheckboxStateDeselected)
, _callback(nullptr)
, _touchesDisabled(false)
{

}

MTCheckbox::~MTCheckbox() {
	_bgSprite = nullptr;
	
	auto dispatcher = this->getEventDispatcher();
	dispatcher->removeEventListener(_listener);
	
	CCLOG("~MTCheckbox deleted");
}

MTCheckbox* MTCheckbox::create(const std::string& selectedFrameName, const std::string& deselectedFrameName, const ccMTCheckBoxCallback& callback) {
	auto node = new (std::nothrow) MTCheckbox();
	if (node && node->init(selectedFrameName, deselectedFrameName, callback)) {
		node->autorelease();
		return node;
	}
	CC_SAFE_DELETE(node);
	return nullptr;
}

bool MTCheckbox::init(const std::string& selectedFrameName, const std::string& deselectedFrameName, const ccMTCheckBoxCallback& callback) {
	if (! Node::init()) {
		return false;
	}
	
	_selectedFrameName = selectedFrameName;
	_deselectedFrameName = deselectedFrameName;
	_callback = callback;
	
	_bgSprite = Sprite::createWithSpriteFrameName(_deselectedFrameName);
	float side = MAX(_bgSprite->getContentSize().width, _bgSprite->getContentSize().height);
	this->setContentSize(Size(side, side));
	_bgSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	_bgSprite->setPosition(mt_center(this));
	this->addChild(_bgSprite);
	
	// Touch setup
	_listener = EventListenerTouchOneByOne::create();
	_listener->setSwallowTouches(true);
	_listener->onTouchBegan = CC_CALLBACK_2(MTCheckbox::onTouchBegan, this);//onTouchBegan = CC_CALLBACK_2(MTCheckbox::onTouchBegan, this);
	auto dispatcher = this->getEventDispatcher();
	dispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
	
	return true;
}

// -------------------------------------
// MARK: General Methods

void MTCheckbox::setSelected(bool selected) {
	_currentState = (selected) ? MTCheckboxStateSelected : MTCheckboxStateDeselected;
	this->updateView();
}

void MTCheckbox::setTouchesDisabled(bool disableTouches) {
	_touchesDisabled = disableTouches;
}

// -------------------------------------
// MARK: Helper Methods

void MTCheckbox::updateView() {
	std::string frameName = (_currentState == MTCheckboxStateSelected) ? _selectedFrameName : _deselectedFrameName;
	_bgSprite->setSpriteFrame(frameName);
}

// -------------------------------------
// MARK: Touches Handling

bool MTCheckbox::onTouchBegan(Touch* touch, Event* event) {
	if (_touchesDisabled) {
		return false;
	}
	
	auto touchStartLocation = touch->getLocation();
	auto localTouchLoc = this->getParent()->convertToNodeSpace(touchStartLocation);
	if (this->getBoundingBox().containsPoint(localTouchLoc)) {
		if (_currentState == MTCheckboxStateDeselected) {
			_currentState = MTCheckboxStateSelected;
		} else {
			_currentState = MTCheckboxStateDeselected;
		}
		this->updateView();
		
		if (_callback) {
			_callback(this);
		}
		
		return true;
	}
	
	return false;
}
