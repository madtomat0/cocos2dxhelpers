//
//  MTButton.hpp
//  SmartMagesBattle
//
//  Created by Paul Gorbunov on 28.11.16.
//
//

#ifndef MTButton_hpp
#define MTButton_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>

class MTButton : public cocos2d::ui::Button {
public:
	static MTButton* create(const std::string &normalImage,
					 const std::string& selectedImage ,
					 const std::string& disableImage,
					 TextureResType texType);
	
	virtual void setEnabled(bool enabled) override;
	
	CC_SYNTHESIZE(bool, _enableSquareTouchArea, EnableSquareTouchArea);
	CC_SYNTHESIZE(GLubyte, _disabledOpacity, DisabledOpacity);
	
	void disableForTimePeriod(float disableTime);
	
	void setBackgroundColor(const cocos2d::Color3B& color);
	
	MTButton();
	virtual ~MTButton();
	
protected:
	
	virtual bool hitTest(const cocos2d::Vec2 &pt, const cocos2d::Camera* camera, cocos2d::Vec3 *p) const override;
	
private:
	bool init(const std::string& normalImage,
			  const std::string& selectedImage,
			  const std::string& disableImage,
			  TextureResType texType) override;
};

#endif /* MTButton_hpp */
