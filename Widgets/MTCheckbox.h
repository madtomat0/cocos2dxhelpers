//
//  MTCheckbox.h
//  LogicIntuition
//
//  Created by MAD Tomato on 19.02.15.
//
//

#ifndef __LogicIntuition__MTCheckbox__
#define __LogicIntuition__MTCheckbox__

#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>

enum MTCheckboxState {
	MTCheckboxStateDeselected = 0,
	MTCheckboxStateSelected
};

typedef std::function<void(cocos2d::Ref*)> ccMTCheckBoxCallback;

class MTCheckbox : public cocos2d::Node {
public:
	~MTCheckbox();
	static MTCheckbox* create(const std::string& selectedFrameName, const std::string& deselectedFrameName, const ccMTCheckBoxCallback& callback);
	
	CC_SYNTHESIZE_READONLY(MTCheckboxState, _currentState, CurrentState);
	void setSelected(bool selected);
	void setTouchesDisabled(bool disableTouches);
	
private:
	
	MTCheckbox();
	bool init(const std::string& selectedFrameName, const std::string& deselectedFrameName, const ccMTCheckBoxCallback& callback);
	
	// Touch
	virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
	
	// Helpers
	void updateView();
	
	std::string _selectedFrameName;
	std::string _deselectedFrameName;
	cocos2d::Sprite* _bgSprite;
	cocos2d::EventListenerTouchOneByOne* _listener;
	bool _touchesDisabled;
	
	ccMTCheckBoxCallback _callback;
};

#endif /* defined(__LogicIntuition__MTCheckbox__) */
