//
//  MTText.h
//  Xonix
//
//  Created by MAD Tomato on 25.12.14.
//
//

#ifndef __Xonix__MTText__
#define __Xonix__MTText__

#include <stdio.h>
#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>

class MTText : public cocos2d::ui::Text {
public:
	MTText();
	~MTText();
	static MTText* create(const std::string &textContent, const std::string &fontName, float fontSize);
	
	void setOnTouchZoom(float zoomValue);

private:
	bool init(const std::string &textContent, const std::string &fontName, float fontSize);
};

#endif /* defined(__Xonix__MTText__) */
