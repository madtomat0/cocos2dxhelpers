//
//  MTActivityIndicator.cpp
//  Xonix
//
//  Created by MAD Tomato on 20.01.15.
//
//

#include "MTActivityIndicator.h"

USING_NS_CC;
using namespace cocos2d::ui;

// -------------------------------------
// MARK: Ctors

MTActivityIndicator::MTActivityIndicator()
: _bgSprite(nullptr)
, _spinner(nullptr)
{
	
}

MTActivityIndicator::~MTActivityIndicator() {
	
	CCLOG("~MTActivityIndicator deleted");
}

// -------------------------------------
// MARK: Initialization

MTActivityIndicator* MTActivityIndicator::create(const std::string& spinnerImageFrameName, const std::string& labelString) {
	MTActivityIndicator* view = new MTActivityIndicator();
	if (view && view->init(spinnerImageFrameName, labelString)) {
		view->autorelease();
		return view;
	}
	CC_SAFE_DELETE(view);
	return nullptr;
}

bool MTActivityIndicator::init(const std::string& spinnerImageFrameName, const std::string& labelString) {
	if (!ui::Layout::init()) {
		return false;
	}
	
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	this->setContentSize(visibleSize);
	this->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	this->setBackGroundColor(Color3B::BLACK);
	this->setOpacity(0);
	this->setTouchEnabled(true);
	this->setPropagateTouchEvents(false);
	
	// Setup bgSprite
	_bgSprite = Sprite::create(MTImgWhiteRectName);
	_bgSprite->setTextureRect(Rect(0, 0, c_indSize.width, c_indSize.height));
	_bgSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	_bgSprite->setNormalizedPosition(Vec2(0.5, 0.5));
	_bgSprite->setColor(Color3B::WHITE);
	_bgSprite->setOpacity(0);
	this->addChild(_bgSprite);
	
	// Setup spinner
	_spinner = Sprite::createWithSpriteFrameName(spinnerImageFrameName);
	_spinner->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	_spinner->setNormalizedPosition(Vec2(0.5, 0.5));
	//_spinner->setOpacity(0);
	this->addChild(_spinner);
	
	// Setup label
	if (labelString != "") {
		auto label = Text::create(labelString, MTMainFont_Regular, 20);
		label->setColor(Color3B::WHITE);
		label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		label->setNormalizedPosition(Vec2(0.5, 0.4));
		this->addChild(label);
	}
	
	return true;
}

// -------------------------------------
// MARK: General Methods

void MTActivityIndicator::showOnNode(cocos2d::Node* parentNode, int zOrder) {
	this->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
	this->setNormalizedPosition(Vec2(0.5, 0.5));
	parentNode->addChild(this, zOrder);
	
	auto fadeTo = FadeTo::create(c_showDuration, c_bgOpacity);
	this->runAction(fadeTo);
	
	auto rotate = RotateBy::create(0.7, -360);
	auto scaleX = ScaleTo::create(0.35, 0.7);
	auto scaleBack = ScaleTo::create(0.35, 1.0);
	auto scaleSeq = Sequence::create(scaleX, scaleBack, nullptr);
	auto spawn = Spawn::create(scaleSeq, rotate, nullptr);
	
	auto repeat = RepeatForever::create(spawn);
	_spinner->runAction(repeat);
}

void MTActivityIndicator::hide() {
	_spinner->stopAllActions();
	this->stopAllActions();
	this->removeFromParentAndCleanup(true);
	
//	_spinner->stopAllActions();
//	_spinner->setOpacity(0);
//	auto fadeOut = FadeOut::create(c_showDuration);
//	auto callback = CallFunc::create([this](){
//		this->removeFromParentAndCleanup(true);
//	});
//	this->runAction(Sequence::create(fadeOut, callback, nullptr));
}

