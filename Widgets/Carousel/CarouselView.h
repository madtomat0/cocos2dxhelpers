//
//  CarouselView.hpp
//  MagicOfTime
//
//  Created by Paul Gorbunov on 24.07.17.
//
//

#ifndef CarouselView_hpp
#define CarouselView_hpp

#include "CarouselViewCell.h"
#include "CarouselCellRecord.h"

class CarouselViewDataSource;
class CarouselViewDelegate;

class CarouselView: public cocos2d::ui::ScrollView {
public:
	~CarouselView();
	
	CREATE_FUNC(CarouselView);
	
	CC_SYNTHESIZE(CarouselViewDataSource*, _dataSource, DataSource);
	CC_SYNTHESIZE(CarouselViewDelegate*, _delegate, Delegate);
	CC_SYNTHESIZE(bool, _snapEnabled, SnapEnabled)
	CC_SYNTHESIZE(float, _snapPointAdjust, SnapVelocityAdjust); // snap point offset based on velocity direction
	CC_SYNTHESIZE(float, _minAutoscrollVelocity, MinAutoscrollVelocity); // pixels per frame
	
	CarouselView* setup(const cocos2d::Size& size, float cellsMargin);
	
	CarouselViewCell* dequeueReusableCellForIdentifier(const std::string& cellIdentifier);
	
	void reloadData();
	
	/**
	 Returns index of cell that is positioned under snap point. If snapping is disabled returns 0.
	 */
	int getIndexOfSnappedCell() const;
    
    int getIndexOfCellContainingWorldPoint(const cocos2d::Vec2& worldPoint) const;
    
    void snapToCellAtIndex(int cellIndex, bool animated);
	
	// Overriden
	virtual void update(float dt) override;
	
protected:
	
	// Overriden
	//virtual void setContentOffset(const cocos2d::Vec2& offset) override;
//    bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unusedEvent) override;
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unusedEvent) override;
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unusedEvent) override;
//    virtual void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *unusedEvent) override;
	
private:
	
	CarouselView();
	
	// Setup
	void generateCellRecordsData();
	void layoutCells();
	void returnNonVisibleCellsToThePool(const cocos2d::ValueVector& currentVisibleIndicies);
	
	// Helpers
	int findCellIndexForOffsetX(float offsetX) const;
	bool vectorContainsIntValue(const cocos2d::ValueVector& vector, int intValue);
    cocos2d::Vec2 findScrollSnapDestinationForCellIndex(int cellIndex) const;
	
	// Snapping
	void checkForSnapPoint();
	void snapToPoint();
	
	cocos2d::Vector<CarouselViewCell*> _reusablePool;
	cocos2d::Vector<CarouselCellRecord*> _cellRecords;
	cocos2d::ValueVector _visibleIndices;
	float _cellsMargin;
	
	cocos2d::Vec2 _snapPoint;
	cocos2d::Vec2 _oldContentOffset;
	cocos2d::Vec2 _velocity; // pixels per frame
	bool _isSnapping;
    bool _bounceHappend;
};

class CarouselViewDataSource {
public:
	virtual int numberOfItems(const CarouselView* carouselView) = 0;
	virtual cocos2d::Size sizeForItemAtIndex(int index, const CarouselView* carouselView) = 0;
	virtual CarouselViewCell* itemForIndex(int index, CarouselView* carouselView) = 0;
};

class CarouselViewDelegate {
public:
	virtual void carouselViewWillBeginMoving(const CarouselView* carouselView) {};
	virtual void carouselViewDidStopMoving(const CarouselView* carouselView) {};
    virtual void carouselViewWillSnapToCellWithIndex(int cellIndex, const CarouselView* carouselView) {};
};

#endif /* CarouselView_hpp */
