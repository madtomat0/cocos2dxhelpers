//
//  CarouselView.cpp
//  MagicOfTime
//
//  Created by Paul Gorbunov on 24.07.17.
//
//

#include "CarouselView.h"
#include "../../../MTDefines.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

CarouselView::CarouselView()
: _reusablePool(Vector<CarouselViewCell*>())
, _cellRecords(Vector<CarouselCellRecord*>())
, _visibleIndices(ValueVectorNull)
, _cellsMargin(0)
, _dataSource(nullptr)
, _delegate(nullptr)
, _snapPoint(Vec2::ZERO)
, _snapEnabled(false)
, _oldContentOffset(Vec2::ZERO)
, _velocity(Vec2::ZERO)
, _isSnapping(false)
, _bounceHappend(false)
, _snapPointAdjust(0)
, _minAutoscrollVelocity(500.0)
{
	
}

CarouselView::~CarouselView() {
	_reusablePool.clear();
	_cellRecords.clear();
	_dataSource = nullptr;
	_delegate = nullptr;
	
	CCLOG("~CarouselView deleted");
}

// -------------------------------------
// MARK: Setup

CarouselView* CarouselView::setup(const cocos2d::Size& size, float cellsMargin) {
	//MTScrollView::setup(size);
	this->setContentSize(size);
	this->setBackGroundColorType(cocos2d::ui::Layout::BackGroundColorType::NONE);
	//this->setBackGroundColor(Color3B::GRAY);
	this->setBounceEnabled(true);
	this->setDirection(ScrollView::Direction::HORIZONTAL);
	this->setClippingEnabled(false);
	
	this->addEventListener([this](Ref* sender, ScrollView::EventType eventType){
		if (eventType == ScrollView::EventType::CONTAINER_MOVED) {
			this->layoutCells();
			this->checkForSnapPoint();
			//CCLOG("CONTAINER_MOVED");
		} else if (eventType == ScrollView::EventType::SCROLLING_BEGAN) {
			_isSnapping = false;
			_bounceHappend = false;
			if (_delegate) {
				_delegate->carouselViewWillBeginMoving(this);
			}
			//CCLOG("SCROLLING_BEGAN");
		} else if (eventType == ScrollView::EventType::SCROLLING_ENDED) {
			//CCLOG("SCROLLING_ENDED");
			if (_velocity.x == 0) {
				this->snapToPoint();
			}
			
		} else if (eventType == ScrollView::EventType::SCROLLING) {
			//CCLOG("SCROLLING");
		} else if (eventType == ScrollView::EventType::AUTOSCROLL_ENDED) {
			//CCLOG("AUTOSCROLL_ENDED");
			_isSnapping = false;
			_bounceHappend = false;
			if (_delegate) {
				_delegate->carouselViewDidStopMoving(this);
			}
		} else if (eventType == ScrollView::EventType::BOUNCE_RIGHT ||
				   eventType == ScrollView::EventType::BOUNCE_LEFT) {
			_bounceHappend = true;
		}
	});
	
	_cellsMargin = cellsMargin;
	
	_snapPoint = mt_center(this);
	
	return this;
}

void CarouselView::generateCellRecordsData() {
	float currentOffsetX = 0;
	
	_cellRecords.clear();
	
	int numberOfItems = _dataSource->numberOfItems(this);
	
	for (int index = 0; index < numberOfItems; index++) {
		auto cellRecord = CarouselCellRecord::create();
		auto cellSize = _dataSource->sizeForItemAtIndex(index, this);
		auto cellPos = Vec2(currentOffsetX + _cellsMargin, 0);
		
		cellRecord->setSize(cellSize);
		cellRecord->setPosition(cellPos);
		
		_cellRecords.insert(index, cellRecord);
		
		currentOffsetX += (cellSize.width + _cellsMargin);
	}
	
	currentOffsetX += _cellsMargin;
	
	this->setInnerContainerSize(Size(currentOffsetX, this->getContentSize().height));
}

void CarouselView::layoutCells() {
	float currentStartX = -this->getInnerContainerPosition().x;
	float currentEndX = currentStartX + this->getContentSize().width;
	
	int indexToDisplay = this->findCellIndexForOffsetX(currentStartX);
	
	auto newVisibleIndices = ValueVectorNull;
	
	float xOrigin = 0;
	float cellWidth = 0;
	float yPos = mt_boundsHeight(this) * 0.5;
	
	do {
		newVisibleIndices.push_back(Value(indexToDisplay));
		
		auto cellRecord = _cellRecords.at(indexToDisplay);
		
		xOrigin = cellRecord->getPosition().x;
		cellWidth = cellRecord->getSize().width;
		auto cell = cellRecord->getCachedCell();
		
		if (!cell) {
			cell = _dataSource->itemForIndex(indexToDisplay, this);
			cellRecord->setCachedCell(cell);
			
			cell->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
			cell->setPosition(Vec2(xOrigin, yPos));
			this->addChild(cell);
		}
		
		indexToDisplay++;
		
	} while (xOrigin + cellWidth < currentEndX && indexToDisplay < _cellRecords.size());
	
	this->returnNonVisibleCellsToThePool(newVisibleIndices);
}

void CarouselView::returnNonVisibleCellsToThePool(const cocos2d::ValueVector& currentVisibleIndicies) {
	for (auto value : _visibleIndices) {
		int oldVisibleIndex = value.asInt();
		if (!this->vectorContainsIntValue(currentVisibleIndicies, oldVisibleIndex)) {
			auto cellRecord = _cellRecords.at(oldVisibleIndex);
			auto cell = cellRecord->getCachedCell();
			if (cell) {
				_reusablePool.pushBack(cell);
				cell->removeFromParentAndCleanup(true);
				cellRecord->setCachedCell(nullptr);
				
				if (cell->getWasRetained()) {
					cell->setWasRetained(false);
					CC_SAFE_RELEASE(cell);
				}
			}
		}
	}
	
	_visibleIndices = currentVisibleIndicies;
}

// -------------------------------------
// MARK: Helpers

int CarouselView::findCellIndexForOffsetX(float offsetX) const {
	int index = 0;
	
	auto cellRecord = CarouselCellRecord::create();
	cellRecord->setPosition(Vec2(offsetX, 0));
	
	auto compareFunc = [](const CarouselCellRecord* record1, const CarouselCellRecord* record2) { return record1->getPosition().x < record2->getPosition().x; };
	auto iter = std::lower_bound(_cellRecords.begin(), _cellRecords.end(), cellRecord, compareFunc);
	
	index = (int)(iter - _cellRecords.begin());
	
	index--;
	index = MAX(0, index);
	
	return index;
}

bool CarouselView::vectorContainsIntValue(const cocos2d::ValueVector& vector, int intValue) {
	for (auto value : vector) {
		if (value.asInt() == intValue) {
			return true;
		}
	}
	
	return false;
}

cocos2d::Vec2 CarouselView::findScrollSnapDestinationForCellIndex(int cellIndex) const {
	if (cellIndex < _cellRecords.size()) {
		auto cellRecord = _cellRecords.at(cellIndex);
		auto destination = Vec2(-cellRecord->getPosition().x + _snapPoint.x - _dataSource->sizeForItemAtIndex(cellIndex, this).width * 0.5, 0);
		
		float minDestX = -(this->getInnerContainerSize().width - this->getContentSize().width);
		if (destination.x < minDestX) {
			destination.x = minDestX;
		} else if (destination.x > 0) {
			destination.x = 0;
		}
		
		return destination;
	}
	
	return Vec2::ZERO;
}

// -------------------------------------
// MARK: Cell Dequeue

CarouselViewCell* CarouselView::dequeueReusableCellForIdentifier(const std::string& cellIdentifier) {
	CarouselViewCell* dequeuedCell = nullptr;
	
	for (auto poolCell : _reusablePool) {
		if (poolCell->getReuseIdentifier() == cellIdentifier) {
			dequeuedCell = poolCell;
			break;
		}
	}
	
	if (dequeuedCell) {
		CC_SAFE_RETAIN(dequeuedCell);
		dequeuedCell->setWasRetained(true);
		_reusablePool.eraseObject(dequeuedCell);
	}
	
	return dequeuedCell;
}

// -------------------------------------
// MARK: General Methods

void CarouselView::reloadData() {
	this->returnNonVisibleCellsToThePool(ValueVectorNull);
	this->generateCellRecordsData();
	this->layoutCells();
}

int CarouselView::getIndexOfSnappedCell() const {
	if (!_snapEnabled) {
		return 0;
	}
	
	// Slightly adjust snap point for velocity direction
	Vec2 snapPoint = _snapPoint;
	if (_velocity.x > 0) {
		snapPoint.x -= _snapPointAdjust;
	} else if (_velocity.x < 0) {
		snapPoint.x += _snapPointAdjust;
	}
	
	auto wordSnapPoint = this->convertToWorldSpace(snapPoint);
	return this->getIndexOfCellContainingWorldPoint(wordSnapPoint);
}

int CarouselView::getIndexOfCellContainingWorldPoint(const cocos2d::Vec2& worldPoint) const {
	auto localSnapPoint = this->getInnerContainer()->convertToNodeSpace(worldPoint);
	
	int closestCellIndex = this->findCellIndexForOffsetX(localSnapPoint.x);
	
	return closestCellIndex;
}

void CarouselView::snapToCellAtIndex(int cellIndex, bool animated) {
	auto scrollDestination = this->findScrollSnapDestinationForCellIndex(cellIndex);
	
	if (_delegate) {
		_delegate->carouselViewWillSnapToCellWithIndex(cellIndex, this);
	}
	
	if (animated) {
		this->startAutoScrollToDestination(scrollDestination, 0.5, true);
	} else {
		this->setInnerContainerPosition(scrollDestination);
	}
}

//// -------------------------------------
//// MARK: Overriden
//
//void CarouselView::setContentOffset(const cocos2d::Vec2& offset) {
//	MTScrollView::setContentOffset(offset);
//	
//	this->layoutCells();
//}

// -------------------------------------
// MARK: Overriden

void CarouselView::update(float dt) {
	ScrollView::update(dt);
	
	_velocity = (this->getInnerContainerPosition() - _oldContentOffset) / dt;
	_oldContentOffset = this->getInnerContainerPosition();
}

//bool CarouselView::onTouchBegan(Touch *touch, Event *unusedEvent) {
//	_delegate->carouselViewWillBeginMoving(this);
//
//	return ScrollView::onTouchBegan(touch, unusedEvent);
//}

void CarouselView::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unusedEvent) {
	if (_delegate) {
		_delegate->carouselViewWillBeginMoving(this);
	}
	
	ScrollView::onTouchMoved(touch, unusedEvent);
}

void CarouselView::onTouchEnded(Touch *touch, Event *unusedEvent) {
	//this->checkForSnapPoint();
	if (abs(_velocity.x) == 0) {
		this->snapToPoint();
	}
	ScrollView::onTouchEnded(touch, unusedEvent);
}
//
//void CarouselView::onTouchCancelled(Touch *touch, Event *unusedEvent) {
//
//	ScrollView::onTouchCancelled(touch, unusedEvent);
//}

// -------------------------------------
// MARK: Snapping

void CarouselView::checkForSnapPoint() {
	if (!_snapEnabled) {
		return;
	}
	
	if (_bounceHappend) {
		return;
	}
	
	if (abs(_velocity.x) <= _minAutoscrollVelocity && this->isAutoScrolling() && !_isSnapping) {
		this->snapToPoint();
	}
}

void CarouselView::snapToPoint() {
	_isSnapping = true;
	
	//CCLOG("Time to snap to point");
	
	int closestCellIndex = this->getIndexOfSnappedCell();
	this->snapToCellAtIndex(closestCellIndex, true);
}
