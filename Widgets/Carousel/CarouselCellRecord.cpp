//
//  CarouselCellRecord.cpp
//  MagicOfTime
//
//  Created by Pavel on 02/08/2017.
//
//

#include "CarouselCellRecord.h"
#include "../../../MTDefines.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

CarouselCellRecord::CarouselCellRecord()
: _position(Vec2::ZERO)
, _size(Size::ZERO)
, _cachedCell(nullptr)
{
	
}

CarouselCellRecord::~CarouselCellRecord() {
	
	CC_SAFE_RELEASE_NULL(_cachedCell);
	
	//CCLOG("~CarouselCellRecord deleted");
}
