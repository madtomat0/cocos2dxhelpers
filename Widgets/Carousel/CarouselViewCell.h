//
//  CarouselViewCell.hpp
//  MagicOfTime
//
//  Created by Paul Gorbunov on 24.07.17.
//
//

#ifndef CarouselViewCell_hpp
#define CarouselViewCell_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>

class CarouselViewCell: public cocos2d::Node {
public:
	
	CREATE_FUNC(CarouselViewCell);
	
	CC_SYNTHESIZE_READONLY_PASS_BY_REF(std::string, _reuseIdentifier, ReuseIdentifier);
    CC_SYNTHESIZE(bool, _wasRetained, WasRetained);
	
	CarouselViewCell* setup(const cocos2d::Size& size, const std::string& reuseIdentifier);
	
protected:
	~CarouselViewCell();
	CarouselViewCell();
	
private:
	//cocos2d::ui::Layout* _testBg;
};

#endif /* CarouselViewCell_hpp */
