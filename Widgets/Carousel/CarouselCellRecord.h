//
//  CarouselCellRecord.hpp
//  MagicOfTime
//
//  Created by Pavel on 02/08/2017.
//
//

#ifndef CarouselCellRecord_hpp
#define CarouselCellRecord_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CarouselViewCell.h"

class CarouselCellRecord : public cocos2d::Ref {
public:
	~CarouselCellRecord();
	
	CREATE_FUNC(CarouselCellRecord);
	
	CC_SYNTHESIZE(cocos2d::Vec2, _position, Position);
	CC_SYNTHESIZE(cocos2d::Size, _size, Size);
	CC_SYNTHESIZE_RETAIN(CarouselViewCell*, _cachedCell, CachedCell);
	
private:
	CarouselCellRecord();
	bool init() {return true;};
	
};

#endif /* CarouselCellRecord_hpp */
