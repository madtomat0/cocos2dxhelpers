//
//  CarouselViewCell.cpp
//  MagicOfTime
//
//  Created by Paul Gorbunov on 24.07.17.
//
//

#include "CarouselViewCell.h"
#include "../../../MTDefines.h"

USING_NS_CC;

// -------------------------------------
// MARK: Ctors

CarouselViewCell::CarouselViewCell()
: _reuseIdentifier("")
//, _testBg(nullptr)
, _wasRetained(false)
{
	
}

CarouselViewCell::~CarouselViewCell() {
	
	CCLOG("~CarouselViewCell deleted");
}

// -------------------------------------
// MARK: Setup

CarouselViewCell* CarouselViewCell::setup(const cocos2d::Size& size, const std::string& reuseIdentifier) {
	_reuseIdentifier = reuseIdentifier;
	
	this->setContentSize(size);
	
//	// Test bg
//	if (!_testBg) {
//		_testBg = MTWidgetsHelper::setupLayout(size, Vec2::ANCHOR_BOTTOM_LEFT, Vec2::ZERO, this, Color3B::GRAY, true);
//		_testBg->setLocalZOrder(1);
//	}
	
	return this;
}
