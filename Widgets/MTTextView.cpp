//
//  MTTextView.cpp
//  Xonix
//
//  Created by MAD Tomato on 15.01.15.
//
//

#include "MTTextView.h"
#include <iostream>
#include "../Managers/MTFontManager.h"
#include <iterator>

USING_NS_CC;
using namespace cocos2d::ui;

const float MTTextViewParams::defaultLineInterval = 4.0;
const Color3B MTTextViewParams::defaultTextColor = Color3B::WHITE;
const Color4B MTTextViewParams::defaultBgColor = Color4B(0, 0, 0, 0);

// -------------------------------------
// MARK: Ctors

MTTextViewParams::MTTextViewParams()
: text("")
, maxWidth(0)
, fontName("")
, fontSize(0)
, textColor(MTTextViewParams::defaultTextColor)
, bgColor(MTTextViewParams::defaultBgColor)
, lineInterval(MTTextViewParams::defaultLineInterval)
, align(MTTextAlignLeft)
{
	
}

MTTextViewParams::MTTextViewParams(const std::string& text, float maxWidth, const std::string& fontName, float fontSize, Color3B textColor, Color4B bgColor, float lineInterval, MTTextAlign align)
: text(text)
, maxWidth(maxWidth)
, fontName(fontName)
, fontSize(fontSize)
, textColor(textColor)
, bgColor(bgColor)
, lineInterval(lineInterval)
, align(align)
{
}

MTTextViewParams::~MTTextViewParams()
{
	CCLOG("~MTTextViewParams deleted");
}

MTTextView::MTTextView()
: _initialBgOpacity(1.0)
{
	
}

MTTextView::~MTTextView() {
	
	CCLOG("~MTTextView deleted");
}

// -------------------------------------
// MARK: Initialization

MTTextView* MTTextView::create(const MTTextViewParams& parameters) {
	MTTextView* view = new MTTextView();
	if (view && view->init(parameters)) {
		view->autorelease();
		return view;
	}
	CC_SAFE_DELETE(view);
	return nullptr;
}

bool MTTextView::init(const MTTextViewParams& parameters) {
	if (!ui::Layout::init()) {
		return false;
	}
	
	_parameters = parameters;
	this->setupView();
	
	return true;
}

// -------------------------------------
// MARK: Setup Methods

void MTTextView::setupView() {
	bool needToUseSpace = MTFontManager::getInstance()->needToUseSpace();
	auto stringsVector = this->divideString(_parameters.text);
	//float scaleFactor = MTScreenHelper::getMenuScaleFactor();
	
	Vector<Text*> lineLabelsVector;
	std::string line = "";
	std::string divider = (needToUseSpace) ? " " : "";
	int triesCount = 0;
	for (int i = 0; i < stringsVector.size(); i++) {
		triesCount++;
		auto nextWord = stringsVector.at(i);
		auto oldLineLabel = Text::create(line, _parameters.fontName, _parameters.fontSize);
		
		bool escapeSymbolFound = (nextWord == "\\n");
		if (escapeSymbolFound) {
			lineLabelsVector.pushBack(oldLineLabel);
			line = "";
			triesCount = 0;
		} else {
			line = StringUtils::format("%s%s%s", line.c_str(), divider.c_str(), nextWord.c_str());
			auto newLineLabel = Text::create(line, _parameters.fontName, _parameters.fontSize);
			if (newLineLabel->getContentSize().width > _parameters.maxWidth) {
				if (triesCount == 1) {
					lineLabelsVector.pushBack(oldLineLabel);
					line = "";
					i--;
				} else if (triesCount > 1) {
					lineLabelsVector.pushBack(newLineLabel);
					triesCount = 0;
				}
				
			} else {
				if (i == stringsVector.size()-1) {
					lineLabelsVector.pushBack(newLineLabel);
				}
				
				triesCount = 0;
			}
		}
		
	}
	
	if (lineLabelsVector.size() != 0) {
		// Set content size
		float lineHeight = lineLabelsVector.at(0)->getContentSize().height;
		this->setContentSize(Size(_parameters.maxWidth, lineLabelsVector.size() * (lineHeight + _parameters.lineInterval)));
		this->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
		this->setBackGroundColor(Color3B(_parameters.bgColor.r, _parameters.bgColor.g, _parameters.bgColor.b));
		this->setOpacity(_parameters.bgColor.a);
		_initialBgOpacity = this->getOpacity();
		
		// Place labels
		int i = 0;
		for (auto lineLabel : lineLabelsVector) {
			if (_parameters.align == MTTextAlignLeft) {
				lineLabel->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
				lineLabel->setPosition(Vec2(0, this->getContentSize().height - i * (lineHeight + _parameters.lineInterval)));
			} else if (_parameters.align == MTTextAlignCenter) {
				lineLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
				lineLabel->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height - i * (lineHeight + _parameters.lineInterval)));
			} else {
				lineLabel->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);
				lineLabel->setPosition(Vec2(this->getContentSize().width, this->getContentSize().height - i * (lineHeight + _parameters.lineInterval)));
			}
			
			this->addChild(lineLabel);
			lineLabel->setColor(_parameters.textColor);
			
			i++;
		}
	}
}

// -------------------------------------
// MARK: Overriden Methods

void MTTextView::setOpacity(GLubyte opacity) {
	if (_initialBgOpacity != 0) {
		Node::setOpacity(opacity);
	}
	
	for (auto child : this->getChildren()) {
		child->setOpacity(opacity);
	}
}

// -------------------------------------
// MARK: Helper Methods

std::vector<std::string> MTTextView::divideString(const std::string& inputString) {
	
	std::stringstream ss(inputString);
	std::istream_iterator<std::string> begin(ss);
	std::istream_iterator<std::string> end;
	std::vector<std::string> vstrings(begin, end);
	std::copy(vstrings.begin(), vstrings.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
	
	return vstrings;
}

// -------------------------------------
// MARK: General Methods

float MTTextView::getActualWidth() {
	float maxWidth = 0;
	for (auto label : this->getChildren()) {
		float labelWidth = mt_boundsWidth(label);
		if ( labelWidth> maxWidth) {
			maxWidth = labelWidth;
		}
	}
	
	return maxWidth;
}
