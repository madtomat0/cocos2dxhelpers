//
//  MTText.cpp
//  Xonix
//
//  Created by MAD Tomato on 25.12.14.
//
//

#include "MTText.h"

USING_NS_CC;
using namespace ui;

MTText::MTText() {
	
}

MTText::~MTText() {
	
}

MTText* MTText::create(const std::string &textContent, const std::string &fontName, float fontSize) {
	MTText* text = new (std::nothrow) MTText();
	if (text && text->init(textContent, fontName, fontSize)) {
		text->autorelease();
		return text;
	}
	CC_SAFE_DELETE(text);
	return nullptr;
}

bool MTText::init(const std::string &textContent, const std::string &fontName, float fontSize) {
	if (! Text::init(textContent, fontName, fontSize)) {
		return false;
	}
	
	return true;
}

void MTText::setOnTouchZoom(float zoomValue) {
	_onSelectedScaleOffset = zoomValue;
}
