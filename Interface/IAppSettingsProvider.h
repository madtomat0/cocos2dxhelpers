//
//  IAppSettingsProvider.h
//  FillTheWords
//
//  Created by Paul Gorbunov on 28/11/2017.
//  Copyright © 2017 Lunapp. All rights reserved.
//

#ifndef IAppSettingsProvider_h
#define IAppSettingsProvider_h

struct IAppSettingsProvider {
	virtual void setAdsDisabled(bool adsDisabled) = 0;
	virtual bool adsDisabled() = 0;
	
	virtual void setHelpWasShown(bool helpWasShown) = 0;
	virtual bool helpWasShown() = 0;
	
	virtual void setSoundEnabled(bool soundEnabled) = 0;
	virtual bool soundEnabled() = 0;
	
	virtual void setMusicEnabled(bool musicEnabled) = 0;
	virtual bool musicEnabled() = 0;
	
	virtual void setSharingEnabled(bool sharingEnabled) = 0;
	virtual bool sharingEnabled() = 0;
	
	virtual std::string getConfigSetting(const std::string& settingKey) = 0;
};

#endif /* IAppSettingsProvider_h */
