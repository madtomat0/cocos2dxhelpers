//
//  MTBaseLayer.cpp
//  SmartMagesBattle
//
//  Created by MAD Tomato on 17.01.15.
//
//

#include "MTBaseLayer.h"

USING_NS_CC;
using namespace cocos2d::ui;

std::string MTBaseLayer::s_activityIndicatorFrameName = "";

// -------------------------------------
// MARK: Ctors

MTBaseLayer::MTBaseLayer()
: _activityIndicator(nullptr)
{
	
}

MTBaseLayer::~MTBaseLayer() {
	
	//CCLOG("~MTBaseLayer deleted");
}

// -------------------------------------
// MARK: General Methods

Scene* MTBaseLayer::embedInScene() {
	auto scene = Scene::create();
	scene->addChild(this);
	
	return scene;
}

void MTBaseLayer::showActivityIndicator(int zOrder, const std::string& indFrameName) {
	if (_activityIndicator) {
		return;
	}
	
	std::string frameName = (indFrameName == "notSet") ? s_activityIndicatorFrameName : indFrameName;
	_activityIndicator = MTActivityIndicator::create(frameName, "");
	_activityIndicator->showOnNode(this, zOrder);
	
}

void MTBaseLayer::hideActivityIndicator() {
	if (_activityIndicator) {
		_activityIndicator->hide();
		_activityIndicator = nullptr;
	}
}

bool MTBaseLayer::activityIndicatorIsActive() {
	return (_activityIndicator != nullptr);
}

// -------------------------------------
// MARK: Overriden Methods

void MTBaseLayer::onEnter() {
	Layer::onEnter();
	
	//add listener
	auto pKeybackListener = EventListenerKeyboard::create();
	pKeybackListener->onKeyReleased = CC_CALLBACK_2(MTBaseLayer::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(pKeybackListener, this);
}

void MTBaseLayer::onExit() {
	//remove listener
	_eventDispatcher->removeEventListenersForTarget(this);
	
	Layer::onExit();
}

// -------------------------------------
// MARK: Keyboard Handling

void MTBaseLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *pEvent) {
	if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
		this->androidBackBtnPressed();
#endif
	}
}
