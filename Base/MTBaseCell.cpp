//
//  MTBaseCell.cpp
//  LogicIntuition
//
//  Created by MAD Tomato on 20.02.15.
//
//

#include "MTBaseCell.h"

USING_NS_CC;
using namespace cocos2d::ui;

// -------------------------------------
// MARK: Ctors

MTBaseCell::MTBaseCell()
: _delegate(nullptr)
, _scaleFactor(1.0)
{
	
}

MTBaseCell::~MTBaseCell() {
	_delegate = nullptr;
	
	//CCLOG("~~MTBaseCell dealloc");
}

// -------------------------------------
// MARK: Setup

void MTBaseCell::setup(MTBaseCellDelegate* delegate, float scaleFactor) {
	_scaleFactor = scaleFactor;
	_delegate = delegate;
	
	this->setTouchEnabled(true);
	this->addTouchEventListener([this](Ref* sender, Widget::TouchEventType eventType){
		
		if (eventType == Widget::TouchEventType::ENDED) {
			_delegate->cellWasTapped(this);
		}
		
	});
}
