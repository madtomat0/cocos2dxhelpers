//
//  MTBaseLayer.h
//  SmartMagesBattle
//
//  Created by MAD Tomato on 17.01.15.
//
//

#ifndef __WordsPuzzle__MTBaseScene__
#define __WordsPuzzle__MTBaseScene__

#include "cocos2d.h"
#include <cocos/ui/CocosGUI.h>
#include "../../MTDefines.h"
#include "../Widgets/MTActivityIndicator.h"

class MTBaseLayer : public cocos2d::Layer {
public:
	virtual void onEnter();
	virtual void onExit();
    
    static std::string s_activityIndicatorFrameName;
	
	cocos2d::Scene* embedInScene();
    
    void showActivityIndicator(int zOrder, const std::string& indFrameName = "notSet");
    void hideActivityIndicator();
    bool activityIndicatorIsActive();
	
protected:
	MTBaseLayer();
	virtual ~MTBaseLayer();
	
	// Methods to Override
	virtual void androidBackBtnPressed() {};
	
private:
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *pEvent);
    
    MTActivityIndicator* _activityIndicator;
};

#endif /* defined(__WordsPuzzle__MTBaseScene__) */
