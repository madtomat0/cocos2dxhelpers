//
//  MTBaseCell.h
//  LogicIntuition
//
//  Created by MAD Tomato on 20.02.15.
//
//

#ifndef __LogicIntuition__MTBaseCell__
#define __LogicIntuition__MTBaseCell__

#include "cocos2d.h"
#include "../../MTDefines.h"
#include <cocos/ui/CocosGUI.h>

class MTBaseCellDelegate;

class MTBaseCell : public cocos2d::ui::Layout {
public:
	
	CREATE_FUNC(MTBaseCell);
	
	// Methods to override
	virtual void updateView() {};
    
    // Setup
    virtual void setup(MTBaseCellDelegate* delegate, float scaleFactor);
	
protected:
	virtual ~MTBaseCell();
	MTBaseCell();
	
	MTBaseCellDelegate* _delegate; // Weak!
	float _scaleFactor;
	
private:
	
};

class MTBaseCellDelegate {
public:
    virtual void cellWasTapped(MTBaseCell* cell) {};
	virtual void cellBtnWasTapped(MTBaseCell* cell, const std::string& btnName) {};
};

#endif /* defined(__LogicIntuition__MTBaseCell__) */
